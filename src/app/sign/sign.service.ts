import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';


export interface LoginContext {
  username: string;
  password: string;
}


@Injectable()
export class SignService {

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService
  ) { }
  signUp(context: LoginContext): Observable<string> {
      let username: string = context.username;
      let password: string = context.password;
      let headers: Headers = new Headers();
      
      return this.http.post('/users', {
          email: context.username,
          password: context.password
      })
      .map((res: Response) => res.json())
      .catch((error) => Observable.throw(error.json().error || error));
  }

  postAccount(firstName: string, lastName: string, email: string) {
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = new RequestOptions({
            //headers: headers,
            params: { access_token: token }
        }); // Create a request option
        const endpoint = '/profiles';
        return this.http
          .post(endpoint, {
            username: this.authenticationService.credentials.username,
            isPublic: true,
            image: 'user image.',
            firstName: firstName,
            lastName: lastName,
            birth: '01/01/1990',
            shortBio: 'Aca podes poner una descripción de tu perfil.',
            adventures: [],
            contributions: [],
            delivery: [],
            email: email
        }, options)
          .map((res: any) => true)
          .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
          //.catch((e:string) => e);
    }
  }

  getAccount(username: string) {
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = new RequestOptions({
            params: { access_token: token }
        }); // Create a request option
        const endpoint = `/profiles/username/${username}`;
        return this.http.get(endpoint, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }
  }

  emailValidation(): Observable<string> {
      return this.http.get('/users/send', {
      })
            .map((res: Response) => res.json())
            .catch(() => Observable.of(':: ERROR ::'));
  }

}
