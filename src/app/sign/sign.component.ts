import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl,
    AbstractControl
} from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { Logger } from '../core/logger.service';
import { SignService } from './sign.service';

const log = new Logger('Login');

@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.scss']
})
export class SignComponent implements OnInit {
    isLoading: boolean;
    queryRoute: string;
    loginForm: FormGroup;
    signUpForm: FormGroup;
    showLogin: boolean;
    showSignUp: boolean;

    error: any;
    alreadyExist: boolean;

    pwdPattern = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,12}$";
    mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
    emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private authenticationService: AuthenticationService,
        private signService: SignService
    ) {
      this.createForm();
    }
    ngOnInit() {
        this.isLoading = false;
        this.showLogin = true;
        this.showSignUp = true;
        this.alreadyExist = false;
        this.route.queryParams.subscribe((params: Response) => {
            this.queryRoute = params['r'] ? params['r'] : '';
        });
    }

    createForm() {
        this.loginForm = this.fb.group({
            username: [ '',
            Validators.compose([
                Validators.required,
                Validators.email,
                Validators.pattern(this.emailPattern)
            ])],
            password: [ '',
            Validators.compose([
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(120)
            ])]
        });

        this.signUpForm = this.fb.group({
            username: [ '',
            Validators.compose([
                Validators.required,
                Validators.email,
                Validators.pattern(this.emailPattern)
            ])],
            firstName: [ '',
            Validators.compose([
                Validators.required,
            ])],
            lastName: [ '',
            Validators.compose([
                Validators.required,
            ])],
            password: [ '',
            Validators.compose([
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(120)
            ])],
            passwordRepeated: [ '',
            Validators.compose([
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(120),
                Validators.pattern(this.pwdPattern)
            ])]
        }, {
            validator: this.passwordMatch
        });
    }

    /**
     * Login Error
     * @param error $Response error
     */
    loginError(error: any) {
        this.isLoading = false;
        this.showLogin = true;
    }

    login() {
        this.isLoading = true;
        this.showLogin = false;
        this.authenticationService.login(this.loginForm.value)
        .subscribe(
            (credentials: any) => {
                this.isLoading = false;
                log.debug(`${credentials} successfully logged in`);
                this.router.navigate([`${this.queryRoute}`], { replaceUrl: true });
                //window.location.href = "/";
        },
        (error: any) => {
           log.debug(`Login error: ${error}`);
           if(!error.ok) this.loginError(error);
           this.error = error;
        });
    }

    signUp() {
      this.isLoading = true;
      this.showSignUp = false;
      this.signService.signUp(this.signUpForm.value)
      .pipe(finalize(() => {
          this.signUpForm.markAsPristine();
      }))
      .subscribe(
          (signUpCredentials: any) => {
              this.authenticationService.login(this.signUpForm.value)
              .subscribe(
                  (credentials: any) => {
                  log.debug(`${credentials} successfully logged in`);

                  if(signUpCredentials) {
                    this.signService.postAccount(
                        this.signUpForm.controls.firstName.value,
                        this.signUpForm.controls.lastName.value,
                        this.signUpForm.controls.email.value,
                    )
                    .subscribe(
                        (res: boolean) => {
                            if(res) {
                                this.router.navigate(['/'], { replaceUrl: true });
                            }
                        },
                        (err: any) => {
                            this.router.navigate(['/error'], { replaceUrl: true });

                        }
                    );
                  } else {
                    this.router.navigate(['/'], { replaceUrl: true });
                  }
              },
                  (error: any) => {
                   log.debug(`Login error: ${error}`);
                   this.error = error;
                   this.router.navigate(['/'], { replaceUrl: true });
              });
          },
          (error: any) => {
              log.debug(`Login error: ${error}`);
              this.error = error;
              if(error.status === 409) {
                  console.log(this.error);
                  this.alreadyExist = true;
                  this.isLoading = false;
                  this.showSignUp = true;
              }
      });
    }

    passwordMatch(c: AbstractControl): { invalid: boolean } {
      if (c.get('password').value !== c.get('passwordRepeated').value) {
          return {invalid: true};
      }
    }
}
