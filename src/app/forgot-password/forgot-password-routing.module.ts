import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { ForgotPasswordComponent } from './forgot-password.component';

const routes: Routes = Route.withShell([
  { path: 'forgot-password', component: ForgotPasswordComponent, data: { title: extract('Forgot Password') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ForgotPasswordRoutingModule { }
