import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  isLoading = false;
  recoveryEmailForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Response) => {
      // if(params['order']) {
      // } else {
      //     this.router.navigate([`/`], { replaceUrl: true });
      // }
    });
  }

  submitRecovery() {
    this.isLoading = true;
  }

  private createForm() {
    this.recoveryEmailForm = this.formBuilder.group({
      email: ['', Validators.required],
    });
  }
}
