import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

export class CreateComponent implements OnInit {
  step: number;
  routeImpulseType: string;
  type: string;

  names: Array<Object>;
  initialState: string;

  impulseIn: boolean;
  impulseOptionsIn: boolean;
  breadcrumbNames: Array<Object>;

  paymentsIn: boolean;
  challengeIn: boolean;
  campaignIn: boolean;
  roundIn: boolean;

  paymentImpulseIn: boolean;
  paymentChallengeIn: boolean;
  paymentCampaignIn: boolean;
  paymentRoundIn: boolean;


  constructor(
      private router: Router,
      private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
      this.impulseIn = (this.router.url.indexOf('/create/impulse') !== -1);

      this.paymentImpulseIn = (this.router.url.indexOf('/create/payments?t=impulse') !== -1);
      this.paymentChallengeIn = (this.router.url.indexOf('/create/payments?t=challenge') !== -1);
      this.impulseOptionsIn = (this.router.url.indexOf('/create/options') !== -1);

      this.paymentsIn = (this.router.url.indexOf('/create/payments') !== -1);

      this.challengeIn = (this.router.url.indexOf('/create/challenge') !== -1);
      this.campaignIn = (this.router.url.indexOf('/create/campaign') !== -1);
      this.roundIn = (this.router.url.indexOf('/create/round') !== -1);

      if(
          this.impulseIn ||
          this.impulseOptionsIn
      ) {
          this.breadcrumbNames = [
              {
                  title: ' ',
                  name: 'presentation',
              },
              {
                title: ' ',
                name: 'participations',
              },
              {
                title: ' ',
                name: 'delivery',
              }
          ];

          (this.impulseIn) ?
              this.initialState = 'presentation' :
              (this.impulseOptionsIn) ?
                this.initialState = 'participations' :
                (this.paymentImpulseIn) ?
                    this.initialState = 'delivery':
                    this.initialState = 'go to another place!';

      } else if(this.challengeIn) {
          this.breadcrumbNames = [
              {
                  title: 'Definition',
                  name: 'definition',
              }
          ];

          this.challengeIn ?
            this.initialState = 'definition' :
              this.initialState = 'go to another place!';
      } else if(this.roundIn) {
          this.breadcrumbNames = [
              {
                  title: 'Investment round',
                  name: 'round',
              }
          ];
          this.roundIn ?
            this.initialState = 'round' :
               this.initialState = 'go to another place!';
      } else if(this.campaignIn) {
          this.breadcrumbNames = [
              {
                  title: 'Make noise!',
                  name: 'campaign',
              }
          ];
          this.roundIn ?
            this.initialState = 'campaign' :
                this.initialState = 'go to another place!';
      }

  }

 }
