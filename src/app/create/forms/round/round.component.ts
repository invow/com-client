import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateService } from '../../create.service';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';


@Component({
  selector: 'app-round',
  templateUrl: './round.component.html',
  styleUrls: ['./round.component.scss']
})
export class RoundComponent implements OnInit {

    stepTwo: FormGroup;
        adventureId: string;
        agreements: any = [];

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private createService: CreateService
  ) {
      this.createForm();
  }

  ngOnInit() {
      this.agreements = this.stepTwo.get('agreements') as FormArray;
  }

  createForm() {
      this.stepTwo = this.fb.group({
          step: ['2', Validators.required ],
          type: ['round', Validators.required ],
          agreements: this.fb.array([ this.createAgreement()])
      });
  }

  createAgreement(): FormGroup {
      return this.fb.group({
        price: ['',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(10000000),
                Validators.pattern(/^\d+$/)
            ])
        ],
        participations: ['',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(10000000),
                Validators.pattern(/^\d+$/)
            ])
        ],
        percentage: ['',
            Validators.compose([
                Validators.required,
                Validators.min(1),
                Validators.max(100),
                Validators.pattern(/^\d+$/)
            ])
        ],
        description: ['',
            Validators.compose([
                Validators.required,
                Validators.minLength(120)
            ])
        ]
      });
  }

  addAgreement(): void {
    this.agreements = this.stepTwo.get('agreements') as FormArray;
    this.agreements.push(this.createAgreement());
  }

  deleteAgreement() {
      if((<FormArray>this.stepTwo.controls['agreements']).controls.length !== 1) {
          (<FormArray>this.stepTwo.controls['agreements']).controls.splice(-1, 1);
          this.stepTwo.value.agreements.splice(-1, 1);
      }
  }

  get allAgreements() { return this.stepTwo.get('agreements'); }

  public findInvalidControls(form: FormGroup) {
      const invalid = [];
      const controls = form.controls;
      for (const name in controls) {
          if (controls[name].invalid) {
              invalid.push(name);
          }
      }
      return invalid;
  }
  nextStep() {
      document.location.href = '/create/payments';
  }

  sendStepTwoService() {
      console.log(this.stepTwo.value);
      this.stepTwo.value.collaborators =
        JSON.stringify(this.stepTwo.value.collaborators);
      this.createService.putStepTwo(this.adventureId, this.stepTwo.value)
      .subscribe(
          res => {
              console.log('Post Step Two:');
              this.nextStep();
          },
          err => {
       this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
          });
  }

  sendStepTwo() {
      if(this.stepTwo.invalid) {
          this.stepTwo.controls.agreemnets.markAsTouched({ onlySelf: true });
      } else {
          this.sendStepTwoService();
      }
  }
}
