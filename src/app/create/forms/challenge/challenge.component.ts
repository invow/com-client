import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateService } from '../../create.service';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';
import { FormsService } from '../../../shared/forms/forms.service';

@Component({
  selector: 'app-challenge',
  templateUrl: './challenge.component.html',
  styleUrls: ['./challenge.component.scss']
})
export class ChallengeComponent implements OnInit {

    stepTwo: FormGroup;
        adventureId: string;
        agreements: any = [];

    editorConfig = {
      editable: true,
      spellcheck: false,
      height: '24rem',
      minHeight: '5rem',
      placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
      translate: 'no',
      imageEndPoint: '',
    };

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private createService: CreateService,
      private formsService: FormsService
  ) {
      this.createForm();
  }

  ngOnInit() {
      this.agreements = this.stepTwo.get('agreements') as FormArray;
  }

  createForm() {
    this.stepTwo = this.formsService.createJobsForm();
  }

  addAgreement(): void {
    this.agreements = this.stepTwo.get('agreements') as FormArray;
    this.agreements.push(this.formsService.createAgreement());
  }

  deleteAgreement() {
      if((<FormArray>this.stepTwo.controls['agreements']).controls.length !== 1) {
          (<FormArray>this.stepTwo.controls['agreements']).controls.splice(-1, 1);
          this.stepTwo.value.agreements.splice(-1, 1);
      }
  }

  get allAgreements() { return this.stepTwo.get('agreements'); }
  get name() { return this.stepTwo.get('name'); }
  get title() { return this.stepTwo.get('title'); }
  get description() { return this.stepTwo.get('description'); }
  get blurb() { return this.stepTwo.get('blurb'); }

  public findInvalidControls(form: FormGroup) {
      const invalid = [];
      const controls = form.controls;
      for (const name in controls) {
          if (controls[name].invalid) {
              invalid.push(name);
          }
      }
      return invalid;
  }
  nextStep() {
      this.router.navigate([`/create/payments?t=challenge`], { replaceUrl: true });
  }

  sendStepOneService() {
      this.createService.postStepOne(this.stepTwo.value)
      .subscribe(
          res => {
              sessionStorage.setItem('adventure', res.id);
              console.log(this.stepTwo.value);
              console.log('Post Step One');
              this.nextStep();
          },
          err => {
       this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
          });
  }

  sendStepOne() {
      if(this.stepTwo.invalid) {
          this.stepTwo.controls.agreemnets.markAsTouched({ onlySelf: true });
      } else {
          this.sendStepOneService();
      }
  }
}
