import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';



import { AuthenticationService } from '../core/authentication/authentication.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


declare global {
    interface FormDataValue {
      uri: string;
      name: string;
      type: string;
    }
  
    interface FormData {
      append(name: string, value: FormDataValue, fileName?: string): void;
      set(name: string, value: FormDataValue, fileName?: string): void;
    }
  }
const routes = {
  quote: (c: RandomQuoteContext) => `/jokes/random?category=${c.category}`
};

export interface RandomQuoteContext {
  // The quote's category: 'nerdy', 'explicit'...
  category: string;
}

@Injectable()
export class CreateService {


  constructor(
      private http: Http,
      private authenticationService: AuthenticationService
  ) { }

      /*  BANK */
      getBanks() {
          let mock = [
              {
                  id: '1',
                  ownerName: 'Federico Crespo',
                  ownerId: '35413939',
                  accountNumber: '2353252039582039580'
              },
              {
                  id: '2',
                  ownerName: 'Piumpsy Bumpsy',
                  ownerId: '10324890',
                  accountNumber: '2353252039582039580'
              },
              {
                  id: '3',
                  ownerName: 'Allegra Pepo',
                  ownerId: '50230598',
                  accountNumber: '2353252039582039580'
              }

          ];
          return mock;
      }

      postBank(form: FormData) {
        console.log(form);
        if(this.authenticationService.isAuthenticated()) {
            const token = this.authenticationService.credentials.token;
            let options       = new RequestOptions({
                //headers: headers,
                params: { access_token: token }
            }); // Create a request option
            const endpoint = '/banks';
            return this.http
              .post(endpoint, form, options)
              .map((res: any) => res.json())
              .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
              //.catch((e:string) => e);
        }
     }

      getPaymentSources() {
          let mock = [
              {
                  id: '3',
                  accountEmail: 'fedechomba@paypal.com',
                  type: 'PayPal'
              },
              {
                  id: '4',
                  accountEmail: 'fedechomba@gmail.com',
                  type: 'MercadoPago'
              },

          ];
          return mock;
      }

      postSource(form: FormData) {
        console.log(form);
        if(this.authenticationService.isAuthenticated()) {
            const token = this.authenticationService.credentials.token;
            let options       = new RequestOptions({
                //headers: headers,
                params: { access_token: token }
            }); // Create a request option
            const endpoint = '/sources';
            return this.http
              .post(endpoint, form, options)
              .map((res: any) => res.json())
              .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
              //.catch((e:string) => e);
        }
     }

      postFile(filesToUpload: File, fileKey: string) {  
        if(this.authenticationService.isAuthenticated()) {
          const token = this.authenticationService.credentials.token;
          let options       = {
              params: { access_token: token },
              reportProgress: true,
              observe: 'events'
          }; // Create a request option
          const endpoint = '/adventures/upload';
          const formData: FormData = new FormData();
          formData.append(
            'image',
            new Blob([filesToUpload[0]], { type: filesToUpload[0].type }),
            filesToUpload[0]['originalname']
          );
          return this.http
            .post(endpoint, formData, options)
            .map((res) => res.json())
            .catch((e:string) => e);
      }
     }

     deleteImage(image: string) {  
      if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = {
            params: { access_token: token },
        }; // Create a request option
        const endpoint = `/adventures/delete/${image}`;
        return this.http
          .get(endpoint, options)
          .map((res) => res.json())
          .catch((e:string) => e);
    }
   }

     postFileData(name: string, path: string, type: string) {
       if(this.authenticationService.isAuthenticated()) {
           const token = this.authenticationService.credentials.token;
           let options       = new RequestOptions({
               params: { access_token: token }
           }); // Create a request option
           const endpoint = '/files';

           return this.http
             .post(endpoint, { name: name, path: path, type: type }, options)
             .map((res: any) => res.json())
             .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
       }
     }

     /* Step One */
     postStepOne(form: FormData) {
       if(this.authenticationService.isAuthenticated()) {
           const token = this.authenticationService.credentials.token;
           let options       = new RequestOptions({
               //headers: headers,
               params: { access_token: token }
           }); // Create a request option
           const endpoint = '/adventures';
           return this.http
             .post(endpoint, form, options)
             .map((res: any) => res.json())
             .catch((error:any) => {
               console.log('ERROR ::: ::: :::');
               console.log(error);
               return Observable.throw(error.json().error || 'Server error')
             }); //...errors if any
             //.catch((e:string) => e);
       }
    }
    putStepTwo(id: string, body: any) { //this.adventures[aindex].delivery
        if(this.authenticationService.isAuthenticated()) {
            const token = this.authenticationService.credentials.token;
            const endpoint = `/adventures/${id}`;
            let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
            let options = new RequestOptions({
                headers: headers,
                params: { access_token: token }
            });
            return this.http.put(endpoint, body, options)
            .map(() => { return true })
            .catch((error:any) => Observable.throw(body));
        }
    }
    putStepThree(id: string, body: FormData) {
        if(this.authenticationService.isAuthenticated()) {
            const token = this.authenticationService.credentials.token;
            const endpoint = `/adventures/${id}`;
            let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
            let options = new RequestOptions({
                headers: headers,
                params: { access_token: token }
            });

            return this.http.put(endpoint, body, options) // ...using post request
                             .map(() => { return true }) // ...and calling .json() on the response to return data
                             .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
        }
    }

    postPayment(form: FormData) {
      if(this.authenticationService.isAuthenticated()) {
          const token = this.authenticationService.credentials.token;
          let options       = new RequestOptions({
              //headers: headers,
              params: { access_token: token }
          }); // Create a request option
          const endpoint = '/payments';
          return this.http
            .post(endpoint, form, options)
            .map((res: any) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
            //.catch((e:string) => e);
      }
    }

    deleteFile(id: string, file: string) {
      if(this.authenticationService.isAuthenticated()) {
          const token = this.authenticationService.credentials.token;
          let options       = new RequestOptions({
              //headers: headers,
              params: { access_token: token }
          }); // Create a request option
          const endpoint = `/files/${id}/${file}`;
          return this.http
            .delete(endpoint, options)
            .map((res: any) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
            //.catch((e:string) => e);
      }
    }
}
