import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

import { AuthenticationService } from '../core/authentication/authentication.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const routes = {
  quote: (c: RandomQuoteContext) => `/jokes/random?category=${c.category}`
};

export interface RandomQuoteContext {
  // The quote's category: 'nerdy', 'explicit'...
  category: string;
}

@Injectable()
export class CheckoutService {
  constructor(
      private http: Http,
      private authenticationService: AuthenticationService
  ) {

  }

  checkout(body: any, phone: number, address: string) {
    if(this.authenticationService.isAuthenticated()) {
      body.phone = phone;
      body.address = address;

        const token = this.authenticationService.credentials.token;
        const endpoint = `/checkouts`;
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({
            headers: headers,
            params: { access_token: token }
        });
        return this.http.post(endpoint, body, options) // ...using post request
                         .map(res => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error)); //...errors if any  error.json().error || body
    }
  }

}
