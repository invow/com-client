import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from '../core/authentication/authentication.guard';
import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { CheckoutComponent } from './checkout.component';

const routes: Routes = Route.withShell([
  {
      path: 'checkout',
      component: CheckoutComponent,
      canActivate: [AuthenticationGuard],
      data: { title: extract('Checkout') }
  }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class CheckoutRoutingModule { }
