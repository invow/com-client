import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from '../core/authentication/authentication.guard';
import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { TermsComponent } from './terms.component';

const routes: Routes = Route.withShell([
  {
      path: 'terms',
      component: TermsComponent,
      data: { title: extract('Terms') }
  }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class TermsRoutingModule { }
