import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';

@Injectable()
export class PriceService {

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService,
  ) { }
  
  getPrices(): Observable<string> {
    if(this.authenticationService.isAuthenticated()) {
      const token = this.authenticationService.credentials.token;
      let options       = new RequestOptions({
          params: { access_token: token }
      }); // Create a request option
      return this.http.get('/prices', options)
      .pipe(
        map((res: Response) => res.json()),
        catchError(() => Observable.of('Error catched trying to get prices'))
      );
    }
  }
}

