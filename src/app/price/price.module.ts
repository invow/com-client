import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { PriceRoutingModule } from './price-routing.module';
import { PriceComponent } from './price.component';
import { ChartsModule } from 'ng2-charts';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { PriceService } from './price.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    PriceRoutingModule,
    ChartsModule
  ],
  declarations: [
    PriceComponent
  ],
  providers: [
    PriceService
  ],
})
export class PriceModule { }
