import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { FormsService } from './forms.service'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';


@Injectable()
export class ProfileFormsService {

  constructor(
      private fb: FormBuilder,
      private formsService: FormsService
  ) { }

  createAccount(accountObject?: any) {
    return this.fb.group({
      isPublic: [
        (accountObject && accountObject.isPublic) ? accountObject.isPublic : '',
        Validators.required
      ],
      image: [
        (accountObject && accountObject.image) ? accountObject.image : '',
        Validators.required
      ],
      firstName: [
        (accountObject && accountObject.firstName) ? accountObject.firstName : '',
        Validators.required
      ],
      lastName: [
        (accountObject && accountObject.lastName) ? accountObject.lastName : '',
        Validators.required
      ],
      shortBio: [
        (accountObject && accountObject.shortBio) ? accountObject.shortBio : '',
        Validators.required
      ],
      phone: [
        (accountObject && accountObject.phone) ? accountObject.phone : '',
        Validators.required
      ],
      money: [
        (accountObject && accountObject.money) ? accountObject.money : '',
        Validators.required
      ],
      publications: this.fb.array([
        this.createPublication(
          (accountObject && accountObject.publications) ? accountObject.publications : ''
        )
      ]),
      reviews: this.fb.array([
        this.formsService.createReview()
      ]),
      documents: this.fb.array([
        this.formsService.createDocs()
      ]),
      skills: this.fb.array([
        this.createSkill(
          (accountObject && accountObject.skills) ? accountObject.skills : ''
        )
      ]),
      campaigns: this.fb.array([
        this.createCampaign(
          (accountObject && accountObject.campaigns) ? accountObject.campaigns : ''
        )
      ]),
      impulses: this.fb.array([
        this.createImpulse(
          (accountObject && accountObject.impulses) ? accountObject.impulses : ''
        )
      ]),
      investments: this.fb.array([
        this.createInvestment(
          (accountObject && accountObject.investments) ? accountObject.investments : ''
        )
      ]),
      jobs: this.fb.array([
        this.createJob(
          (accountObject && accountObject.jobs) ? accountObject.jobs : ''
        )
      ]),
      guruAgents: this.fb.array([
        this.createGuruAgent(
          (accountObject && accountObject.guruAgents) ? accountObject.guruAgents : ''
        )
      ]),
      birthday: [
        (accountObject && accountObject.birthday) ? accountObject.birthday : '',
        Validators.required
      ],
      state: [
        (accountObject && accountObject.state) ? accountObject.state : '',
        Validators.required
      ],
      city: [
        (accountObject && accountObject.city) ? accountObject.city : '',
        Validators.required
      ],
      country: [
        (accountObject && accountObject.country) ? accountObject.country : '',
        Validators.required
      ],
      email: [
        (accountObject && accountObject.email) ? accountObject.email : '',
        Validators.compose([
          Validators.required,
          Validators.email
        ])
      ]
    });
  }

  createPublication(publicationsObject?: any): FormGroup {
      if(publicationsObject) {
        return publicationsObject;
      } else {
        return this.fb.group({
            index: [
              (publicationsObject && publicationsObject.index) ? publicationsObject.index : 1,
              Validators.required
            ],
            publication: [
              (publicationsObject && publicationsObject.publication) ? publicationsObject.publication : '',
              Validators.required
            ]
        });
      }
  }

  createSkill(skillsObject?: any): FormGroup {
    if(skillsObject) {
      return skillsObject;
    } else {
      return this.fb.group({
          index: [
            (skillsObject && skillsObject.index) ? skillsObject.index : 1,
            Validators.required
          ],
          skill: [
            (skillsObject && skillsObject.skill) ? skillsObject.skill : '',
            Validators.required
          ],
          description: [
            (skillsObject && skillsObject.description) ? skillsObject.description : '',
          ]
      });
    }
  }

  createCampaign(campaignObject?: any): FormGroup {
    if(campaignObject) {
      return campaignObject;
    } else {
      return this.fb.group({
          index: [
            (campaignObject && campaignObject.index) ? campaignObject.index : 1,
            Validators.required
          ],
          campaign: [
            (campaignObject && campaignObject.campaign) ? campaignObject.campaign : '',
            Validators.required
          ]
      });
    }
  }

  createImpulse(impulseObject?: any): FormGroup {
    if(impulseObject) {
      return impulseObject;
    } else {
      return this.fb.group({
          index: [
            (impulseObject && impulseObject.index) ? impulseObject.index : 1,
            Validators.required
          ],
          impulse: [
            (impulseObject && impulseObject.impulse) ? impulseObject.impulse : '',
            Validators.required
          ],
          iam: [
            (impulseObject && impulseObject.iam) ? impulseObject.iam : '',
            Validators.required
          ],
          name: [
            (impulseObject && impulseObject.name) ? impulseObject.name : '',
            Validators.required
          ],
          goal: [
            (impulseObject && impulseObject.goal) ? impulseObject.goal : '',
            Validators.required
          ],
          fundingType: [
            (impulseObject && impulseObject.fundingType) ? impulseObject.fundingType : 1,
            Validators.required
          ],
          guruAgent: [
            (impulseObject && impulseObject.fundingType) ? impulseObject.fundingType : 1,
            Validators.required
          ]
      });
    }
  }

  createInvestment(investmentObject?: any): FormGroup {
    if(investmentObject) {
      return investmentObject;
    } else {
      return this.fb.group({
          index: [
            (investmentObject && investmentObject.index) ? investmentObject.index : 1,
            Validators.required
          ],
          name: [
            (investmentObject && investmentObject.name) ? investmentObject.name : 1,
            Validators.required
          ],
          description: [
            (investmentObject && investmentObject.description) ? investmentObject.description : 1,
            Validators.required
          ],
          amount: [
            (investmentObject && investmentObject.amount) ? investmentObject.amount : '',
            Validators.required
          ],
          goalAmount: [
            (investmentObject && investmentObject.goalAmount) ? investmentObject.goalAmount : '',
            Validators.required
          ],
          goalCategory: [
            (investmentObject && investmentObject.goalCategory) ? investmentObject.goalCategory : '',
            Validators.required
          ],
          goalPlace: [
            (investmentObject && investmentObject.goalPlace) ? investmentObject.goalPlace : '',
            Validators.required
          ],
          goalMethod: [
            (investmentObject && investmentObject.goalMethod) ? investmentObject.goalMethod : '',
            Validators.required
          ]
      });
    }
  }

  createJob(jobObject?: any): FormGroup {
    if(jobObject) {
      return jobObject;
    } else {
      return this.fb.group({
          index: [
            (jobObject && jobObject.index) ? jobObject.index : 1,
            Validators.required
          ],
          job: [
            (jobObject && jobObject.job) ? jobObject.job : '',
            Validators.required
          ],
          jobType: [
            (jobObject && jobObject.jobType) ? jobObject.jobType : '',
            Validators.required
          ],
          country: [
            (jobObject && jobObject.country) ? jobObject.country : '',
            Validators.required
          ],
          city: [
            (jobObject && jobObject.city) ? jobObject.city : '',
            Validators.required
          ]
      });
    }
  }

  createGuruAgent(guruAgentObject?: any): FormGroup {
    if(guruAgentObject) {
      return guruAgentObject;
    } else {
      return this.fb.group({
        fullName: [(guruAgentObject && guruAgentObject.fullName) ? guruAgentObject.fullName : '',
        Validators.required],
        identification: [(guruAgentObject && guruAgentObject.identification) ? guruAgentObject.identification : '',
        Validators.required],
        email: [(guruAgentObject && guruAgentObject.email) ? guruAgentObject.email : '',
        Validators.required],
        phone: [(guruAgentObject && guruAgentObject.phone) ? guruAgentObject.phone : '',
        Validators.required],
        resume: [(guruAgentObject && guruAgentObject.resume) ? guruAgentObject.resume : '',
        Validators.required]
      });
    }
  }

  createChannel(channelObject?: any): FormGroup {
    if(channelObject) {
      return channelObject;
    } else {
      return this.fb.group({
        index: [(channelObject && channelObject.index) ? channelObject.index : '',
        Validators.required],
        name: [(channelObject && channelObject.name) ? channelObject.name : '',
        Validators.required],
        link: [(channelObject && channelObject.link) ? channelObject.link : '',
        Validators.required],
        category: [(channelObject && channelObject.category) ? channelObject.category : '',
        Validators.required],
        description: [(channelObject && channelObject.description) ? channelObject.description : '',
        Validators.required]
      });
    }
  }

  createLink(linkObject?: any): FormGroup {
    if(linkObject) {
      return linkObject;
    } else {
      return this.fb.group({
        link: [(linkObject && linkObject.link) ? linkObject.link : '']
      });
    }
  }

  createAnnouncement(announcementObject?: any): FormGroup {
    if(announcementObject) {
      return announcementObject;
    } else {
      return this.fb.group({
        index: [(announcementObject && announcementObject.index) ? announcementObject.index : '',
        Validators.required],
        title: [(announcementObject && announcementObject.title) ? announcementObject.title : '',
        Validators.required],
        category: [(announcementObject && announcementObject.category) ? announcementObject.category : '',
        Validators.required],
        description: [(announcementObject && announcementObject.description) ? announcementObject.description : '',
        Validators.required],
        days: [(announcementObject && announcementObject.days) ? announcementObject.days : '',
        Validators.required],
        views: [(announcementObject && announcementObject.views) ? announcementObject.views : '',
        Validators.required]
      });
    }
  }

  createTransfer(transferObject?: any): FormGroup {
    if(transferObject) {
      return transferObject;
    } else {
      return this.fb.group({
        destiny: [(transferObject && transferObject.destiny) ? transferObject.destiny : '',
        Validators.required],
        amount: [(transferObject && transferObject.amount) ? transferObject.amount : '',
        Validators.required]
      });
    }
  }

  createTransferReport(transferReportObject?: any): FormGroup {
    if(transferReportObject) {
      return transferReportObject;
    } else {
      return this.fb.group({
        amount: [(transferReportObject && transferReportObject.amount) ? transferReportObject.amount : '',
        Validators.required],
        bank: [(transferReportObject && transferReportObject.bank) ? transferReportObject.bank : '',
        Validators.required],
        date: [(transferReportObject && transferReportObject.date) ? transferReportObject.date : '',
        Validators.required],
        hour: [(transferReportObject && transferReportObject.hour) ? transferReportObject.hour : '',
        Validators.required],
        accountNumber: [(transferReportObject && transferReportObject.accountNumber) ? transferReportObject.accountNumber : '',
        Validators.required],
        image: [(transferReportObject && transferReportObject.image) ? transferReportObject.image : '']
      });
    }
  }




}
