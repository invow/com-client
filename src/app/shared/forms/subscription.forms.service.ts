import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { FormsService } from './forms.service'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';


@Injectable()
export class SubscriptionFormsService {

  constructor(
      private fb: FormBuilder,
      private formsService: FormsService
  ) { }

  createSubscription(subscriptionObject?: any) {
    return this.fb.group({
      email: [
        (subscriptionObject && subscriptionObject.firstName) ? subscriptionObject.firstName : '',
        Validators.compose([
          Validators.required,
          Validators.email
        ])
      ]
    });

  }
}
