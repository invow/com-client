import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { Router } from '@angular/router';
import { result } from 'lodash';


@Injectable()
export class AdventureService {

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService,
    private router: Router
    ) { }

  getCurrentUser() {
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = new RequestOptions({
            params: { access_token: token }
        }); // Create a request option
        const endpoint = `/users/me`;
        return this.http.get(endpoint, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }
  }

  like(advId: String, likes: any, like: boolean) {
    if(like)
      likes.push({ user: this.authenticationService.credentials.username });
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options = new RequestOptions({
            headers: headers,
            params: { access_token: token }
        });
        const endpoint = `/adventures/${advId}`;
        return this.http
          .put(endpoint, { likes: likes }, options)
          .map((res: any) => res.json())
          .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
          //.catch((e:string) => e);
    } else {
      this.router.navigate(['/sign'], { replaceUrl: true });
    }
 }

  getNewest() {
    const endpoint = '/adventures';
    return this.http.get(endpoint)
      .map(res => {
        const advStorage = JSON.stringify(res.json());
        if(sessionStorage.getItem('adventures')) sessionStorage.removeItem('adventures');
        sessionStorage.setItem('adventures', advStorage);
        return res.json();
      })
      .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
  }

  getAdventure(id: string) {
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = new RequestOptions({
            params: { access_token: token }
        }); // Create a request option
        const endpoint = `/adventures/${id}`;
        return this.http.get(endpoint, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

  }

  getByQ(field: string, val: string) {
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = new RequestOptions({
            params: {
              access_token: token,
              type: "impulse",
            }
        }); // Create a request option
        const endpoint = `/adventures` //&sort=${field} //q=${field}
        return this.http.get(endpoint, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }
  }

  getByInvcode(invcode: string) {
    const endpoint = `/adventures/invcode/${invcode}` //&sort=${field} //q=${field}
    return this.http.get(endpoint)
      .map(res => res.json())
      .catch(error => Observable.throw(error.json() || 'Server error')); //...errors if any
  }

  replaceLocalAdventure(invcode: string, adventure: any) {
    const localAdventures = JSON.parse(sessionStorage.getItem('adventures'));
    if(localAdventures) sessionStorage.removeItem('adventures');
    localAdventures.forEach((item: any, index: number) => {
      if(item.invcode.toString() === invcode) localAdventures[index] = adventure;
    });
    sessionStorage.setItem('adventures', JSON.stringify(localAdventures));
  }

  getByInvcodeStorage(invcode: string): any {
    let result = null;
    const adventures = JSON.parse(sessionStorage.getItem('adventures'));
    adventures.forEach((item: any) => {
      if(item.invcode.toString() === invcode)
        result = item;
    });
    return result;
  }

  getByType(type: string) {
    if(this.authenticationService.isAuthenticated()) {
        const token = this.authenticationService.credentials.token;
        let options       = new RequestOptions({
            params: {
              access_token: token
            }
        });
        const endpoint = `/adventures/type/${type}`; //&sort=${field} //q=${field}
        return this.http.get(endpoint, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }
  }

  getBySlug(slug: string) {

    ////////////
    var obj = [
      {"name": "Afghanistan", "code": "AF"},
      {"name": "Åland Islands", "code": "AX"},
      {"name": "Albania", "code": "AL"},
      {"name": "Algeria", "code": "DZ"}
    ];

    // the code you're looking for
    var needle = 'AL';

    // iterate over each element in the array
    for (var i = 0; i < obj.length; i++){
      // look for the entry with a matching `code` value
      if (obj[i].code == needle){
         // we found it
        // obj[i].name is the matched result
      }
    }
    this.getNewest()
        .subscribe(
            res => {
                console.log(res);
            },
            err => {
              this.router.navigate([`/error`], { replaceUrl: true });
            }
        )
  }



}
