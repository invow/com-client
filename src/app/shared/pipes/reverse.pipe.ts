// capitalize.pipe.ts

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'reverse'})
export class ReversePipe implements PipeTransform {
  transform(arr: any) {
    var copy = arr.slice();
    return copy.reverse();
  }
  //Capitalize
  // transform(value: string, args: string[]): any {
  //   if (!value) return value;
  //
  //   return value.replace(/\w\S*/g, function(txt) {
  //       return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  //   });
  // }
}
