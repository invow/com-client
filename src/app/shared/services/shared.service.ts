import { Injectable, isDevMode } from '@angular/core';
import { Http } from '@angular/http';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { environment as environmentProd } from '../../../environments/environment.prod';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class SharedService {
  areVisible: boolean = true;
  verticals: any;
  headerColor = '#fff';
  public isLoading: boolean = true;
  public chartData: any[] = [];
  public chartLabel: any[] = [];

  constructor(
      private http: Http,
      private router: Router
  ) { }

  /**
   * Get Image from Blob Storage
   * @param name String
   */
  getImage(name: string) {
    return `${this.getDomain()}/${name}`;
  }

  /**
   * GET HEADER COLOR
   */
  getHeaderColor() {
    return this.headerColor;
  }

  /**
   * CHANGE HEADER COLOR
   */
  changeHeaderColor() {
    return this.headerColor = '#030102';
  }

  /**
   * IS VISIBLE
   */
  isVisible() {
    return this.areVisible;
  }

  /**
   * HIDE TOOLBAR
   */
  hideToolbar() {
    return this.areVisible = false;
  }
  
  /**
   * SHOW TOOLBAR
   */
  showToolbar() {
    return this.areVisible = true;
  }

  /**
   * UPDATE CHART DATA
   * @param action String
   * @param val Number
   */
  updateChartData(action: string, val: number) {
    if(action === 'add') {
      this.chartData.push(val);
    } else if(action === 'remove') {
      this.chartData.splice(-1, 1);
    }
  }

  /**
   * UPDATE CHART LABEL
   * @param action String
   * @param val String
   */
  updateChartLabel(action: string, val: string) {
    if(action === 'add') {
      this.chartLabel.push(val);
    } else if(action === 'remove') {
      this.chartLabel.splice(-1, 1);
    }
  }

  /**
   * POST SUBSCRIPTION
   * @param form $FormData
   */
  postSubscription(form: FormData) {
    const endpoint = '/subscriptions';
    return this.http
      .post(endpoint, form)
      .map((res: any) => res.json())
      .catch((error:any) => {
        console.log('ERROR ::: ::: :::');
        console.log(error);
        return Observable.throw(error.json().error || 'Server error')
      }); //...errors if any
      //.catch((e:string) => e);
  }

  /**
   * TO NUMBER
   * @param chars String
   */
  toNumber(chars: string): number {
    return parseInt(chars);
  }

  /**
   * IMAGE EXISTS
   * @param url String
   * @param callback Function
   */
  imageExists(url: string, callback: Function) {
    let img = new Image();
    img.onload = () => callback(true);
    img.onerror = () => callback(false);
    //img.src = url;
  }

  /**
   * GO TO ADVENTURE
   * @param invcode String
   * @param slug String
   */
  goToAdventure(invcode: string, slug: string) {
    this.router.navigate([`adventure/${invcode}/${slug}`], { replaceUrl: true });
  }

  /**
   * FIND ITEM
   * @param item: *
   * @param index: Number
   * @param initialContent: * 
   */
  findItem(
    item: any,
    initialContent: any
  ) 
  {
    return item !== initialContent;
  }

  /**
   * FIND INVALID CONTROLS
   * @param form $FormGroup
   */
  public findInvalidControls(form: FormGroup) {
    const invalid = [];
    const controls = form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  /**
   * VALIDATE FILE
   * @param fileType String
   * @param name String 
   * @param size Number
   * @param maxSize Number
   */
  validateFile(fileType: string, name: String, size: number, maxSize: number) {
    let ext = name.substring(name.lastIndexOf('.') + 1);
    let extLower = ext.toLowerCase();
    let fileSize = null;
    let fileExtension = null;
    if(fileType === 'doc') {
        fileExtension = (
          extLower === 'docx' ||
          extLower === 'pptx' ||
          extLower === 'xlsx' ||
          extLower === 'pdf'
        );
        fileSize = (
            size <= maxSize
        );
    }
    return (fileExtension && fileSize) ?
          true : false;
  }

  /**
   * GET DOMAIN
   */
  getDomain() {
    let domain = '';
    if(isDevMode())
      domain = environment.assetsUrl;
    else 
      domain = environmentProd.assetsUrl;
    return domain;
  }
}
