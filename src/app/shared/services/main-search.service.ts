import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class SearchService {
  baseUrl: string = '/results';
  queryUrl: string = '?fields=name,image,blurb,days,invcode,slug&limit=100';

  constructor(private http: Http) { }

  search(terms: Observable<string>) {
    return terms.debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(term));
  }

  searchEntries(term: string) {
    let headers      = new Headers({ searcher: true }); // ... Set content type to JSON
    let options = new RequestOptions({
        headers: headers
    });
    return this.http
        .get(this.baseUrl + this.queryUrl)
        .map(res => res.json());
  }
}