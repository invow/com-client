import { Injectable } from '@angular/core';
import { CartItem, Currency } from '../classes/cart-item';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class CartService {
    cart : CartItem[];
    added: boolean;

    constructor() {
        this.loadCart();
        this.added = false;
    }

    loadCart(){
      this.cart = [];
        if (sessionStorage.getItem("cart"))
            this.cart = JSON.parse(sessionStorage.getItem("cart"))
    }
    add(cartItem: CartItem, maxStock?: number){
        var _t = this
        var found: boolean = false;
        this.cart.forEach(function(item){
            if (cartItem._id == item._id){
                found = true;
                if (cartItem.type == "participation" || cartItem.type == "reward"){
                    item.quantity += cartItem.quantity
                    if (maxStock){
                        if (item.quantity > maxStock){
                            item.quantity = maxStock
                        }
                    }
                    _t.added = true;
                    setTimeout(function(){
                        _t.added = false
                    }, 3000)
                }
            }
        })
        if (!found){
            this.cart.push(cartItem)
            sessionStorage.setItem("cart", JSON.stringify(this.cart))
            this.added = true;
            setTimeout(function(){
                _t.added = false
            }, 3000)
        }
        console.log(this.cart)
    }
    
    remove(i: number){
        this.cart.splice(i, 1);
        sessionStorage.setItem("cart", JSON.stringify(this.cart))
    }
    clear(){
        this.cart = []
    }
    get(){
        return this.cart;
    }
    subtotals(){
        var currencies: Currency[] = []
        var cur : Array<any> = []
        this.cart.forEach(function(item){
            cur[item.currency] = cur[item.currency] ? cur[item.currency] + item.price * item.quantity : item.price * item.quantity;
        })
        Object.keys(cur).forEach(function(c){
            currencies.push({
                code: c,
                value: cur[c]
            })
        })
        return currencies;
    }
    length(){
        return this.cart.length;
    }
    isVisible(){
        return this.added;
    }
}
