import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl,
    AbstractControl
} from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Logger } from '../../core/logger.service';
import { AuthenticationService } from '../../core/authentication/authentication.service';
import { SharedService } from '../services/shared.service';
import { SignService } from '../../sign/sign.service';
import { UserFormsService } from '../forms/user.forms.service';

const log = new Logger('Login');

@Component({
  selector: 'app-main-home',
  templateUrl: './main-home.component.html',
  styleUrls: ['./main-home.component.scss']
})
export class MainHomeComponent implements OnInit {
  isLoading: boolean;
  account: any;
  loginForm: FormGroup;
  signUpForm: FormGroup;
  firstForm: FormGroup;
  uploadState: string;
  uploadingState: string;
  signupFormView: boolean;
  signupSuccess: boolean;
  signupError: boolean;
  domain: string;

  firstFormView: boolean;
  firstFormSuccess: boolean;
  firstFormError: boolean;
  error: string;
  slideSelected: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public sharedService: SharedService,
    private authenticationService: AuthenticationService,
    private signService: SignService,
    private userFormsService: UserFormsService
  ) {

  }

  getRandomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  ngOnInit() {
    this.domain = this.sharedService.getDomain();
    this.isLoading = true;
    this.slideSelected = this.getRandomInt(0, 1);
    if(this.username) {
      this.signService.getAccount(this.username)
      .subscribe(
        res => {
          this.isLoading = false;
          this.account = res;
          this.firstFormView = res.firstForm ? false : true;
        },
        err => {
          this.firstFormView = true;
          console.log(this.firstFormView);
          if(this.firstFormView) {
            this.sharedService.hideToolbar();
          }
          //this.router.navigate([`/error?err=${err}`], { replaceUrl: true });
        }
      );
    } else {
      this.isLoading = false;
    }

    this.createForms();
    this.uploadState = 'Tu foto';
    this.uploadingState = 'Subiendo...';
    this.signupSuccess = false;
    this.signupError = false;
    this.signupFormView = true;

    this.firstFormSuccess = false;
    this.firstFormError = false;

  }

  get username(): string {
    const credentials = this.authenticationService.credentials;
    return credentials ? credentials.username : null;
  }

  apiMessage(field: string) {
    switch(field) {
      case 'signupSuccess':
      this.signupFormView = false;
      this.signupError = false;
      this.signupSuccess = true;
      break;
      case 'firstFormSuccess':
      this.firstFormView = false;
      this.firstFormError = false;
      this.firstFormSuccess = true;
      break;
    }
    setTimeout(() => {
      switch(field) {
        case 'signupSuccess':
        this.signUpForm.reset();
        this.signupSuccess = false;
        this.signupFormView = false;
        this.firstFormView = true;
        if(this.firstFormView && !this.signupFormView && !this.firstFormSuccess) {
          this.sharedService.hideToolbar();
        }
        break;
        case 'firstFormSuccess':
        this.firstForm.reset();
        this.firstFormSuccess = false;
        this.firstFormView = false;
        break;
      }
    }, 6000);
  }

  createForms() {
      this.loginForm = this.userFormsService.createLoginForm();
      this.signUpForm = this.userFormsService.createSignUpForm();
      this.firstForm = this.userFormsService.createFirstForm();
  }

  login() {
      this.isLoading = true;
      this.authenticationService.login(this.loginForm.value)
      .subscribe(
          (credentials: any) => {
              this.isLoading = false;
              log.debug(`${credentials} successfully logged in`);
              this.router.navigate([`/`], { replaceUrl: true });
              //window.location.href = "/";
      },
      (error: any) => {
         log.debug(`Login error: ${error}`);
         this.error = error;
      });
  }

  signUp() {
    this.signupFormView = false;
    this.isLoading = true;
    this.signService.signUp(this.signUpForm.value)
    .pipe(finalize(() => {
        this.signUpForm.markAsPristine();
    }))
    .subscribe(
        credentials => {
          /*
            this.signService.emailValidation()
            .subscribe(
                (Ok: any) => {
                    console.log("Mail sent!");
                },
                (err: any) => {
                    console.log(err);
                }
            )
            */
            this.authenticationService.login(this.signUpForm.value)
            .subscribe(
                (credentials: any) => {
                this.isLoading = false;
                log.debug(`${credentials} successfully logged in`);
                //this.router.navigate(['/'], { replaceUrl: true });
                //window.location.href = "/";//
                window.scroll(0, 0);
                this.apiMessage('signupSuccess');
            },
                (error: any) => {
                 log.debug(`Login error: ${error}`);
                 this.error = error;
                 this.signupFormView = true;

            });
        },
        error => {
            log.debug(`Login error: ${error}`);
            this.error = error;
            this.signupFormView = true;
    });
  }

  goToProfile() {
    this.isLoading = true;
  //   this.signService.postAccount(this.firstForm.value)
  //   .subscribe(
  //     res => {
  //       this.isLoading = false;
  //       window.scroll(0 ,0);
  //       this.apiMessage('firstFormSuccess');
  //       document.location.href = `/${this.username}`;
  //     },
  //     err => {
  //  this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
  //     }
  //   );
  }

  goUser(param: string) {
    this.router.navigate(['/' + param]);
  }

  goToInvest() {
    if(this.username) {
      this.router.navigate(['/guru/invest/categories'], { replaceUrl: true });
    } else {
      this.router.navigate(['/sign'], { replaceUrl: true });
    }

  }

  goToRewards() {
    this.router.navigate(['/rewards'], { replaceUrl: true })
  }

}
