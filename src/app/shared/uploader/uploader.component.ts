import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from '../../core/authentication/authentication.service';
import { SharedService } from '../services/shared.service';
import { CreateService } from '../../create/create.service';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss']
})
export class UploaderComponent implements OnInit {

  fileUploaded: boolean;
  fileExtension: boolean;
  @Input() fileForm: FormGroup;
  @Input() control: string;
  @Input() type: string;
  @Input() uploadState: string;
  @Input() uploadingState: string;
  @Input() required: boolean;
  @Input() size: number;

  fileRequired: boolean;
  fileSize: boolean;
  fileModel: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private fb: FormBuilder,
    public sharedService: SharedService,
    private createService: CreateService,
  ) {
  }

  ngOnInit() {
    this.fileUploaded = false;
    this.fileExtension = false;
    this.fileRequired = false;
  }

  validateFile(fileType: string, name: String, maxSize: number) {
    let ext = name.substring(name.lastIndexOf('.') + 1);
    let extLower = ext.toLowerCase();
    switch(fileType) {
      case 'doc':
      this.fileExtension = (
          extLower === 'docx' ||
          extLower === 'pptx' ||
          extLower === 'xlsx' ||
          extLower === 'pdf'
      );
      break;
      case 'image':
      this.fileExtension = (
          extLower === 'jpg' ||
          extLower === 'png' ||
          extLower === 'jpeg' ||
          extLower === 'bmp'
      );
      break;
      case 'video':
      this.fileExtension = (
          extLower === 'mp4' ||
          extLower === 'mkv' ||
          extLower === 'm4v' ||
          extLower === 'webm' ||
          extLower === 'ogv'
      );
      break;
    }
    this.fileSize = (this.size <= maxSize);
    return (this.fileExtension && this.fileSize) ? true : false;
  }

  onFileChange($event: any) {
      let files = $event.target.files; // <--- File Object for future use.
      if(
          $event.target.files &&
          files &&
          this.validateFile(
            this.type,
            files[0].name,
            this.size)
          ) {
            this.uploadState = this.uploadingState;
            this.fileForm.get(this.control).setValue(
              files ? files[0].name : ''
            );
            /**
             * @postFile
             */
            this.createService.postFile(files, this.type)
            .subscribe(
                res => {
                    this.createService.postFileData(
                      files[0].name,
                      res ? res.name : 
                      'Error while tried to find the name generated in adventure/uploads.',
                      this.type
                    )
                    .subscribe(
                      res => {
                        console.log('Success!');
                        console.log(res);
                      },
                      err => {
                        this.router.navigate(['/error'], {
                          queryParams: { err: JSON.stringify(err) }
                        });
                      }
                    );
                    this.uploadState = files[0].name;
                    this.fileForm.controls[this.control].setValue(res ? res.name : '');
                    this.fileUploaded = true;
                },
                err => {
                  this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                });
      }
  }

  get username(): string {
    const credentials = this.authenticationService.credentials;
    return credentials ? credentials.username : null;
  }

}
