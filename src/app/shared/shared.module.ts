import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ClickStopPropagation } from './directives/click-stop-propagation/click-stop-propagation.directive';
import { LoaderComponent } from './loader/loader.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { UploaderComponent } from './uploader/uploader.component';
import { MainHomeComponent } from './main-home/main-home.component';
import { ProfileSideComponent } from './profile-side/profile-side.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { AuthenticationService } from '../core/authentication/authentication.service';
import { AuthenticationGuard } from '../core/authentication/authentication.guard';
import { SharedService } from './services/shared.service';
import { CartService } from './services/cart.service';
import { CreateService } from '../create/create.service';
import { UserFormsService } from './forms/user.forms.service';
import { SignService } from '../sign/sign.service';
import { SearchService } from './services/main-search.service';
import { CountriesService } from './services/countries.service';
import { PipeModule } from '../shared/pipes/pipe.module'; // import our pipe here



@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    PipeModule
  ],
  declarations: [
    LoaderComponent,
    ClickStopPropagation,
    ToolbarComponent,
    UploaderComponent,
    MainHomeComponent,
    ProfileSideComponent
  ],
  exports: [
    LoaderComponent,
    ClickStopPropagation,
    ToolbarComponent,
    UploaderComponent,
    MainHomeComponent,
    ProfileSideComponent
  ],
  providers: [
    AuthenticationService,
    AuthenticationGuard,
    CreateService,
    SharedService,
    CartService,
    UserFormsService,
    SignService,
    SearchService,
    CountriesService,
  ]
})
export class SharedModule { }
