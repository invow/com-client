import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdventureService } from '../shared/adventure.service';
import { FormsService } from '../shared/forms/forms.service';
import { DomSanitizer, SafeUrl, Title, Meta } from '@angular/platform-browser';
import { SharedService } from '../shared/services/shared.service';
import { Account } from '../shared/classes/account';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { AccountService } from '../shared/services/account.service'
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-adventure',
  templateUrl: './adventure.component.html',
  styleUrls: ['./adventure.component.scss']
})
export class AdventureComponent implements OnInit {
  title = 'Adventure by Invow';
  domain: string;
  accounts: Account[];
  currentUser: boolean;
  job: boolean;
  currentInvcode: string;
  currentSlug: string;
  adventure: any;
  urlEmbed: SafeUrl;
  adventureForm: FormGroup;
  adventureReviewsResult: number;
  currentPercentage: number;
  styleExpression :string;
  showAll: boolean;

  constructor(
    private route: ActivatedRoute,
    private adventureService: AdventureService,
    private formsService: FormsService,
    private sanitizer: DomSanitizer,
    public sharedService: SharedService,
    private accountService: AccountService,
    private authenticationService: AuthenticationService,
    private titleService: Title,
    private metaTagService: Meta
  ) {
    this.createAdventureForm();
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.sharedService.isLoading = true;
    this.adventureReviewsResult = 0;
    this.showAll = false;

    this.domain = this.sharedService.getDomain();

    /**
     * @Route Params
     */
    this.route.params.subscribe((params: Response) => {
      this.currentInvcode = params['invcode'] ? params['invcode'] : 'error';
      this.currentSlug = params['slug'] ? params['slug'] : 'error';
    });

    const adventure = this.adventureService.getByInvcodeStorage(this.currentInvcode);
    this.AdventureInitialPayload(adventure, this);
  }


  /**
   * Account Initial Iteration
   * @param account Account
   */
  AccountInitialIteration (account: any, _t: this): any {
    if (account.currency == "ARS"){
      _t.adventure.capital = account.amount;
      _t.currentPercentage = _t.adventure.capital * 100 / _t.adventure.goal;
      _t.styleExpression = String(_t.currentPercentage) + '%';
    }
  }
  
  /**
   * Account Initial Payload
   * @param res Response
   */
  AccountInitialPayload(res: any, vm: this) {
    res.forEach( (account: any) => { vm.AccountInitialIteration(account, vm) } );
  }

  /**
   * GET CURRENT USER
   * @param res Response
   */
  GetCurrentUser(res: any, vm: this) {
    vm.sharedService.isLoading = false;
    vm.showAll = true;
    vm.currentUser = (vm.adventure.user.id === res.id);
  }

  /**
   * Get Output Field
   * @param type String
   * @param value String
   */
  getField(type: string, value: string) {
    this.showAll = false;
    switch(type) {
      case 'title':
        this.adventure.title = value;
      break;
      case 'blurb':
        this.adventure.blurb = value;
      break;
      case 'description':
        if(value !== 'chart') this.adventure.description = value;
      break;
      case 'review':
        this.adventure.reviews = value;
        this.refreshReviews(this.adventure.reviews);
      break;
    }
  }

  /**
   * Adventure Initial Payload
   * @param res Response
   */
  AdventureInitialPayload(res: any, vm: this) {
    vm.sharedService.isLoading = false;
    if(this.currentSlug === res.slug && res.ready) {
      this.adventure = res;

      this.setMetaTags();
      this.accountService.byOwner(this.adventure.id).subscribe(
        res => { this.AccountInitialPayload(res, this) }
      );

      if(this.username) {
        this.adventureService.getCurrentUser().subscribe(
          res => { this.GetCurrentUser(res, this) },
          err => { if(err) throw err }
        );
      } else {
        this.currentUser = false;
      }

      this.refreshReviews(this.adventure.reviews);
      this.urlEmbed = this.sanitizer.bypassSecurityTrustResourceUrl(
        this.adventure.embed + '?autoplay=1&controls=0&autohide=1&showinfo=0&loop=1&rel=0'
      );
    }
  }

  /**
   * SET META TAGS
   * @Meta tags
   */
  setMetaTags() {
    this.titleService.setTitle(this.adventure.title);
    
    //OG
    this.metaTagService.addTag({ property: 'og:title', content: this.adventure.title });
    this.metaTagService.addTag({ property: 'og:description', content: this.adventure.blurb });
    this.metaTagService.addTag({ property: 'og:image', content: `${this.domain}/image/thumb/${this.adventure.img}` });
    this.metaTagService.addTag({ property: 'og:url', content: `${window.location.origin}/adventure/${this.adventure.invcode}/${this.adventure.slug}` });

    //TWITTER
    this.metaTagService.addTag({ name: 'twitter:title', content: this.adventure.title });
    this.metaTagService.addTag({ name: 'twitter:description', content: this.adventure.blurb });
    this.metaTagService.addTag({ name: 'twitter:image', content: `${this.domain}/image/thumb/${this.adventure.img}` });
    this.metaTagService.addTag({ name: 'twitter:card', content: this.adventure.blurb });
  }

  /**
   * CREATE ADVENTURE FORM
   */
  createAdventureForm() {
    this.adventureForm = this.formsService.createImpulseForm();
  }

  /**
   * REFRESH REVIEWS
   * @param reviews *
   */
  refreshReviews(reviews: any) {
    if(reviews && reviews.length !== 0) {
      reviews.forEach((item: any, index: number) => {
        this.adventureReviewsResult = this.adventureReviewsResult + Number(item.stars) / reviews.length;
      })
    }
  }
  /**
   * @GETTER
   */
  get username(): string {
    const credentials = this.authenticationService.credentials;
    return credentials ? credentials.username : null;
  }
}