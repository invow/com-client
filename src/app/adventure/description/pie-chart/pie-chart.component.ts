import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { AdventureService } from '../../../shared/adventure.service';
import { FormsService } from '../../../shared/forms/forms.service';
import { SharedService } from '../../../shared/services/shared.service';
import { CreateService } from '../../../create/create.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {
  @Input() adventureCharts: any = null;
  @Input() adventureId: string = null;
  @Input() adventureForm: FormGroup = null
  @Input() currentInvcode: string = null;
  @Input() currentSlug: string = null;
  @Input() currentUser: boolean = null;
  @Output() getChart = new EventEmitter<string>();

  chartsForm: FormGroup;
  charts: any = [];
  currentChart: string;

  // Pie
  public pieChartLabels:string[];
  public pieChartData:number[];
  public pieChartType:string = 'pie';
  public pieChartOptions:any = {
    legend: {
      position:'bottom'
    }
  };
  showChart: boolean;
  showChartView: boolean;
  showChartInputs: boolean;
  chartTitle: string;
  chartDescription: string;
  chartData: number[];
  chartLabels: string[];

  chartTitleView: string;
  chartDescriptionView: string;
  labels: any = [];
  allData: any = [];
  totalActions: number;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private adventureService: AdventureService,
      private formsService: FormsService,
      private createService: CreateService,
      public sharedService: SharedService,
      private fb: FormBuilder,

  ) {
    this.createChartsForm();
  }

  ngOnInit() {
    this.charts = this.chartsForm.get('charts') as FormArray;
    this.pieChartData = [
      this.charts.value[0].amount
    ];
    this.pieChartLabels = [
      this.charts.value[0].destiny
    ];
    this.totalActions = 1;
    this.showChart = false;
    this.showChartView = false;
    this.chartTitleView = '';
    this.chartDescriptionView = '';
    this.chartTitle = 'Add a pie chart!';
    this.chartDescription = 'Partí tu funding goal y dale diversos destinos!';
    this.showChartInputs = false;

    let vm = this;
    if(this.adventureCharts && this.adventureCharts !== []) {
      this.adventureCharts.forEach(
      (mainItem: any, mainIndex: number) => {
        vm.allData[mainIndex] = [];
        vm.labels[mainIndex] = [];
        mainItem.charts.forEach(
          (item: any, index: number) => {
            vm.allData[mainIndex].push(item.amount);
            vm.labels[mainIndex].push(item.destiny);
          }
        );
      });
      this.chartTitleView = this.adventureCharts.title;
      this.chartDescriptionView = this.adventureCharts.description;
      this.showChartView = true;
    }
  }

  /**
   * Emit Chart value to Adventure Component
   * @param value String
   */
  emitChart() {
    this.getChart.emit('chart');
  }

  /**
   * CREATE CHARTS FORM
   */
  createChartsForm() {
    this.chartsForm = this.formsService.createChartsForm();
  }

   /**
   * REFRESH CHART
   */
  refreshChart() {
    this.chartTitle = this.chartsForm.get('title').value;
    this.chartDescription = this.chartsForm.get('description').value;
    this.charts = this.chartsForm.get('charts') as FormArray;
    this.charts.controls.forEach((item: any, index: number) => {
      this.pieChartData[index] = item.controls.amount.value;
      this.pieChartLabels[index] = item.controls.destiny.value;
    })
    this.showChart = true;
  }

  /**
   * SHOW CHART PREVIEW
   */
  showChartPreview() {
    this.showChart = false;
    this.showChartView = false;
  }

  /**
   * ADD CHART
   */
  addChart(): void {
    this.showChart = false;
    this.charts = this.chartsForm.get('charts') as FormArray;
    this.charts.push(this.formsService.createChartForm());
    this.pieChartData.push(
      this.chartsForm.get('charts').get(String(this.charts.length - 1)).get('amount').value
    );
    this.pieChartLabels.push(
      this.chartsForm.get('charts').get(String(this.charts.length - 1)).get('destiny').value
    );
  }

  /**
   * DELETE CHART
   */
  deleteChart(): void {
    this.showChart = false;
    if((<FormArray>this.chartsForm.controls['charts']).controls.length !== 1) {
        (<FormArray>this.chartsForm.controls['charts']).controls.splice(-1, 1);
        this.chartsForm.value.charts.splice(-1, 1);
        this.pieChartData.splice(-1, 1);
        this.pieChartLabels.splice(-1, 1);
    }
  }

  /**
   * CHART CLICKED
   * @param e: $Event
   */
  public chartClicked(e:any):void {
    console.log(e);
  }

  /**
   * CHART HOVERED
   * @param e: $Event
   */
  public chartHovered(e:any):void {
    console.log(e);
  }

  /**
   * TOGGLE INPUT
   * @param field: string
   */
  toggleInput(field :string) {
    this.showChartInputs = this.showChartInputs ? false : true;
  }

   /**
   * EDIT
   * @param field {String}
   * @param initialContent {*}
   */
  edit(field: string, initialContent: any) {
    this.adventureForm = this.formsService.createImpulseForm(initialContent);
    this.chartsForm = this.formsService.createChartsForm(initialContent);
    this.charts = this.chartsForm.get('charts') as FormArray;
    let vm = this;
    initialContent.charts.forEach((item: any, index: number) => {
      this.charts.push(this.formsService.createChartForm(item));
    });
    this.currentChart = initialContent._id;
    (<FormGroup>this.adventureForm).removeControl('imageInput');
    this.toggleInput(field);
  }

  /**
   * REMOVE
   * @param field {String}
   * @param initialContent {*}
   */
  remove(field: string, initialContent: any) {
    let toSend: any;
      var found = this.adventureCharts.filter(this.sharedService.findItem);
      this.adventureCharts = found;
      toSend = {
        charts: this.adventureCharts
      }
      this.createService.putStepTwo(this.adventureId, toSend)
      .subscribe(res => { console.log(res) }, err=> {});
  }

  /**
   * SAVE
   * @param type? {String}
   * @param content? {*}
   * @param index? {String}
   */
  save(
    type?: string,
    content?: any,
    index?: string
  ) {
    this.sharedService.isLoading = true;
    this.emitChart();
    let toSend: any;
    if(!content) {
        this.showChartInputs = false;
        this.showChartView = false;
        this.adventureCharts.push(this.chartsForm.value);
        toSend = {
          charts: this.adventureCharts
        };
    } else {
        this.showChartInputs = false;
        this.showChartView = false;
        this.adventureCharts[index] = this.chartsForm.value;
        toSend = {
          charts: this.adventureCharts
        };
        this.toggleInput(type);
    }
    //after save:
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(
      res => {
        this.sharedService.isLoading = false;
        this.adventureService.getByInvcode(this.currentInvcode)
            .subscribe(
                res => {
                    if(this.currentSlug === res.slug) {
                      this.chartsForm.reset();
                      this.charts = this.fb.array([this.formsService.createChartForm()]);
                      this.showChart = false;
                      this.chartTitle = 'Add a pie chart!';
                      this.chartDescription = 'Partí tu funding goal y dale diversos destinos!';
                      this.showChartInputs = false;
                      this.showChartView = true;

                      if(this.adventureCharts.length !== 0) {
                        this.showChartView = true;
                      }

                      let vm = this;
                      if(this.adventureCharts && this.adventureCharts !== []) {
                        this.adventureCharts.forEach(function(mainItem: any, mainIndex: number){
                          vm.allData[mainIndex] = [];
                          vm.labels[mainIndex] = [];
                          mainItem.charts.forEach((item: any, index: number) => {
                            vm.allData[mainIndex].push(item.amount);
                            vm.labels[mainIndex].push(item.destiny);
                          });
                        })

                        this.chartTitleView = this.adventureCharts.title;
                        this.chartDescriptionView = this.adventureCharts.description;
                        this.showChartView = true;
                      }
                    }
                    else { alert("INCORRECT URL") }
                },
                err => {
                  this.router.navigate([`/error?err=${err}`, err], { replaceUrl: true });
                }
            )

      },
      err => {
        this.router.navigate([`/error?err=${err}`, err], { replaceUrl: true });
      }
    )
  }


  /**
   * CANCEL
   * @param field String
   */
  cancel(field: string) {
    this.toggleInput(field);
    (<FormGroup>this.adventureForm).removeControl(field);
    this.chartsForm.reset();
    this.charts = this.fb.array([this.formsService.createChartForm()]);
    this.showChart = false;
    this.chartTitle = 'Add a pie chart!';
    this.chartDescription = 'Partí tu funding goal y dale diversos destinos!';
  }

  /**
   * @GETTER
   * @Forms
   */
  get allCharts() { return this.chartsForm.get('charts'); }
}
