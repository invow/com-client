import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { CoreModule } from '../../../core/core.module';
import { SharedModule } from '../../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    ChartsModule
  ],
  declarations: [
  ],
  providers: [
  ]
})
export class PieChartModule { }
