import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  displayLabel: boolean = false;
  @Input() capital: number = null;
  @Input() goal: number = null;
  @Input() createdAt: Date = null;
  @Input() endDate: Date = null;

  days: number = null;

  constructor(
      private router: Router,
      private route: ActivatedRoute
  ) { }

  ngOnInit() {
    let diffTime = Math.abs(
      new Date(this.endDate).getTime() -
      new Date(this.createdAt).getTime()
    );

    this.days = Math.ceil(
      diffTime /
      (1000 * 60 * 60 * 24)
    );


  }
}
