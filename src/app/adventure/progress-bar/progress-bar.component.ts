import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdventureService } from '../../shared/adventure.service';
import { AccountService } from '../../shared/services/account.service'
import { SharedService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  @Input() adventureGoal: number = null;
  @Input() adventureCapital: number = null;
  @Input() adventureBackers: number = null;
  @Input() adventureDays: number = null;
  @Input() currentUser: boolean = null;
  @Input() currentInvcode: string = null;
  @Input() currentSlug: string = null;
  @Input() currentPercentage: any = null;
  @Input() styleExpression: string = null;
  @Input() showAll: boolean = null;
  @Input() adventureEndDate: any = 0;

  days: any;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private adventureService: AdventureService,
      private accountService: AccountService,
      private sharedService: SharedService
  ) { }

  calculateDays(date: Date, days: any) {
    var result = new Date(date);
    result.setDate(result.getDate() - days);
    return result;
  }

  ngOnInit() {
    let diffTime = Math.abs(
      new Date(this.adventureEndDate).getTime() -
      new Date().getTime()
    );

    const days = Math.ceil(
      diffTime /
      (1000 * 60 * 60 * 24)
    );
    this.days = days  > this.adventureDays ? 0 : days;
  }
}
