import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-meta',
  templateUrl: './meta.component.html',
  styleUrls: ['./meta.component.scss']
})
export class MetaComponent implements OnInit {
  @Input() adventureLocation: string = null;
  @Input() adventureCategory: string = null;
  @Input() adventureReviewsResult: number = null;

  constructor(
      private router: Router,
      private route: ActivatedRoute
  ) { }

  ngOnInit() {  }
}
