import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CreateService } from '../../create/create.service';
import { Router } from '@angular/router';
import { SharedService } from '../../shared/services/shared.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-blurb',
  templateUrl: './blurb.component.html',
  styleUrls: ['./blurb.component.scss']
})
export class BlurbComponent implements OnInit {
  @Input() adventureBlurb: string = null;
  @Input() adventureId: string = null;
  @Input() currentUser: boolean = null;
  @Input() currentSlug: boolean = null;
  @Input() currentInvcode: string = null;
  @Input() adventureForm: FormGroup
  showBlurbInput: boolean;
  showBlurbEditButton: boolean;
  @Output() getBlurb = new EventEmitter<string>();

  constructor(
      private router: Router,
      private createService: CreateService,
      public sharedService: SharedService,
  ) { }

  ngOnInit() {
    this.showBlurbEditButton = false;
    this.showBlurbInput = false;
  }

  /**
   * Emit Blurb value to Adventure Component
   * @param value String
   */
  emitBlurb(value: string) {
    this.getBlurb.emit(value);
  }

  /**
   * SAVE
   * @param type? {String}
   * @param content? {*}
   * @param index? {String}
   */
  save() {
    this.sharedService.isLoading = true;
    this.emitBlurb(this.blurb.value);
    let toSend: any = {
      blurb: this.blurb.value
    };
    //after save:
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(
      (success: boolean) => {
        if(success) {
          this.sharedService.isLoading = false;
          this.showBlurbInput = false;
        }
      },
      err => {
        this.router.navigate([`/error?err=${err}`, err], { replaceUrl: true });
      }
    )
  }

  /**
   * TOGGLE INPUT
   * @param field: string
   */
  toggleInput(field :string) {
    this.showBlurbInput = this.showBlurbInput ? false : true;
  }

  /**
   * CANCEL
   * @param field String
   */
  cancel(field: string) {
    this.toggleInput(field);
    this.showBlurbEditButton = false;
  }

  /**
   * EDIT
   * @param field {String}
   * @param initialContent {*}
   */
  edit(field: string, initialContent: any) {
    this.adventureForm.controls.blurb.setValue(initialContent);
    (<FormGroup>this.adventureForm).removeControl('imageInput');
    this.toggleInput(field);
  }

  /**
   * @GETTER
   * @Forms
   */
  get blurb() { return this.adventureForm.get('blurb'); }
}
