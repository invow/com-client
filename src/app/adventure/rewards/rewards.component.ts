import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormsService } from '../../shared/forms/forms.service';
import { SharedService } from '../../shared/services/shared.service';
import { CreateService } from '../../create/create.service';
import { AdventureService } from '../../shared/adventure.service';
import { CartService } from '../../shared/services/cart.service';
import { CartItem } from '../../shared/classes/cart-item'

@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.scss']
})
export class RewardsComponent implements OnInit {
  @Input() adventureRewards: any = null;
  @Input() adventureEndDate: Date = null;
  @Input() adventureId: string = null;
  @Input() adventureDays: number = null;
  @Input() adventureInvcode: string = null;
  @Input() adventureSlug: string = null;
  @Input() currentUser: boolean = false;
  @Input() username: string = null;
  @Input() adventureForm: FormGroup = null;

  createCrowdfundingInputs: boolean;
  showCrowdfundingInputs: boolean;
  showCrowdfundingList: boolean;
  crowdfundingForm: FormGroup;
  currentIndex: number;
  days: number;
  reward90: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private createService: CreateService,
    private formsService: FormsService,
    private sharedService: SharedService,
    private adventureService: AdventureService,
    private cartService: CartService
  ) { }

  ngOnInit() {
    this.reward90 = this.sharedService.getImage('reward-90.png');
    this.createCrowdfundingForm();
    this.showCrowdfundingList = false;
    this.createCrowdfundingInputs = false;
    this.showCrowdfundingInputs = false;
    this.days = this.calculateDays();
  }

  /**
   * EDIT
   * @param field {String}
   * @param initialContent {*}
   */
  edit(field: string, initialContent: any) {
    this.crowdfundingForm = this.formsService.createCrowdfundingAgreement(initialContent);
    this.currentIndex = initialContent.index;
    this.toggleInput(field);
  }

  /**
   * REMOVE
   * @param field {String}
   * @param initialContent {*}
   */
  remove(field: string, initialContent: any) {
    let toSend: any;
    var found = this.adventureRewards.filter(this.sharedService.findItem);
    this.adventureRewards = found;
    toSend = {
      rewards: this.adventureRewards
    }
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(res => { console.log(res) }, err=> {});
  }

  /**
   * ADD
   * @param field {String}
   */
  add(field: string) {
    this.crowdfundingForm = this.formsService.createCrowdfundingAgreement();
    this.crowdfundingForm.get('index')
    .setValue(this.adventureRewards.length + 1);
    this.toggleInputCreate(field);
  }

  /**
   * ADD REWARD TO CART
   * @param reward: *
   * @param adv: *
   */
  addReward2Cart(reward: any, adventureInvcode: string, adventureSlug: string){
    if(this.username) {
      var newCartItem: CartItem = {
        _id: reward._id,
        adventure_id: this.adventureId,
        title: reward.title,
        price: reward.pledgeAmount,
        type: "reward",
        quantity: 1,
        currency: reward.currency || "",
        url: `/adventure/${adventureInvcode}/${adventureSlug}`
      };

      this.cartService.add(newCartItem);
    } else {
      this.router.navigate(['/sign'], { replaceUrl: true });
    }
  }

  /**
   * CHECKOUT
   */

  checkout(reward: any) {
    if(this.username) {
      const item =
      [{
        adventure_id: this.adventureId,
        currency: reward.currency || "",
        price: reward.pledgeAmount,
        quantity: 1,
        title: reward.title,
        type: 'reward',
        url: `/adventure/${this.adventureInvcode}/${this.adventureSlug}`,
        _id: reward._id
      }];

      this.router.navigate(['/checkout'], { queryParams: { item: JSON.stringify(item) }});
    } else {
      this.router.navigate(['/sign'], { replaceUrl: true });
    }
  }

  

  /**
   * SAVE
   * @param type? {String}
   * @param content? {*}
   * @param index? {String}
   */
  save(
    type?: string,
    content?: any,
    index?: string
  ) {
    this.sharedService.isLoading = true;
    let toSend: any;
    if(!content) {
      switch(type) {
        case 'crowdfunding':
        this.crowdfundingForm.value.createdDate = new Date();
        this.adventureRewards.push(this.crowdfundingForm.value);
        toSend = {
          rewards: this.adventureRewards
        };
        this.toggleInputCreate(type);
        break;
      }
    } else {
      switch(type) {
        case 'crowdfunding':
        this.adventureRewards[index] = this.crowdfundingForm.value;
        toSend = {
          rewards: this.adventureRewards
        };
        this.toggleInput(type);
        break;
      }
    }
    //after save:
    this.createService.putStepTwo(
      this.adventureId, toSend
    )
    .subscribe(
      res => {
        this.sharedService.isLoading = false;
        this.adventureService.getByInvcode(this.adventureInvcode)
        .subscribe(
          res => {
            if(this.adventureSlug === res.slug) {
              this.adventureRewards =
                this.adventureRewards ?
                  this.adventureRewards: [];
              this.showCrowdfundingList = this.adventureRewards.length >= 1 ?
              true : false;
            } else alert("INCORRECT URL")
          },
          err => {
            this.router.navigate(
              [`/error?err=${err}`],
              { replaceUrl: true }
            );
          }
        )
      },
      err => {
        this.router.navigate(
          [`/error?err=${err}`],
          { replaceUrl: true }
        );
      }
    )
  }

  /**
   * GET DAYS
   * @param createdDate: Date
   * @param days: number
   */
  getDays(createdDate: Date, days: number){
    var a = new Date().getTime();
    var b = new Date(createdDate);
    b = new Date(b.setDate(b.getDate() + days));
    var c = b.getTime() - a;
    var day = (1000 * 60 * 60 * 24)
    var hour = (1000 * 60 * 60)
    var minute = (1000 * 60)
    var second = (1000)
    var days = Math.trunc(c / day)
    var hours = Math.trunc((c - (days * day)) / hour)
    var minutes = Math.trunc((c - (days * day) - (hours * hour)) / minute);
    var seconds = Math.trunc((c - (days * day) - (hours * hour) - (minutes * minute)) / second);
    return {
      days: days,
      hours: hours,
      minutes: minutes,
      seconds: seconds
    }
  }

  /**
   * CALCULATE DAYS
   */
  calculateDays() {
    const diffTime = Math.abs(
      new Date(this.adventureEndDate).getTime() -
      new Date().getTime()
    );

    const days = Math.ceil(
      diffTime /
      (1000 * 60 * 60 * 24)
    );
    const result = days > this.adventureDays ? 0 : days;
    return result;
  }

  /**
   * CANCEL CRATE
   * @param field: string
   */
  cancelCreate(field: string) {
    this.toggleInputCreate(field);
  }

  /**
   * CANCEL
   * @param field String
   */
  cancel(field: string) {
    this.toggleInput(field);
    (<FormGroup>this.adventureForm).removeControl(field);
    this.showCrowdfundingInputs = false;
  }

  /**
   * TOGGLE INPUT
   * @param field string
   */
  toggleInput(field :string) {
      this.showCrowdfundingInputs = this.showCrowdfundingInputs ? false : true;
  }

  /**
   * TOGGLE INPUT CREATE
   * @param field: string
   */
  toggleInputCreate(field :string) {
    this.createCrowdfundingInputs = this.createCrowdfundingInputs ? false : true;
  }

  /**
   * CREATE CROWDFUNDING FORM
   */
  createCrowdfundingForm() {
      this.crowdfundingForm = this.fb.group({});
  }

  /**
   * @GETTERS
   * @Forms 
   */
  get cfPledgeAmount() { return this.crowdfundingForm.get('pledgeAmount'); }
  get cfTitle() { return this.crowdfundingForm.get('title'); }
  get cfDescription() { return this.crowdfundingForm.get('description'); }
  get cfShippingTime() { return this.crowdfundingForm.get('shippingTime'); }
  get cfQuantity() { return this.crowdfundingForm.get('quantity'); }
  get cfDays() { return this.crowdfundingForm.get('days'); }
}

