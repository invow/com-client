import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CartService } from '../../shared/services/cart.service';
import { CartItem } from '../../shared/classes/cart-item';


@Component({
  selector: 'app-donations',
  templateUrl: './donations.component.html',
  styleUrls: ['./donations.component.scss']
})
export class DonationsComponent implements OnInit {
  @Input() adventureId: string = null;
  @Input() adventureInvcode: string = null;
  @Input() adventureSlug: string = null;
  @Input() adventureDays: number = null;
  @Input() adventureTitle: string = null;
  @Input() adventureCurrency: string = null;
  @Input() username: string = null;
  @Input() adventureEndDate: Date = null;

  amount: number;
  days: number;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private cartService: CartService
  ) { }

  ngOnInit() {
    this.days = this.calculateDays();
  }

  /**
   * CALCULATE DAYS
   */
  calculateDays() {
    const diffTime = Math.abs(
      new Date(this.adventureEndDate).getTime() -
      new Date().getTime()
    );

    const days = Math.ceil(
      diffTime /
      (1000 * 60 * 60 * 24)
    );
    const result = days > this.adventureDays ? 0 : days;
    return result;
  }

  /**
   * CHECKOUT
   */
  checkout() {
    if(this.username) {
      const item =
      [{
        adventure_id: this.adventureId,
        currency: "",
        price: this.amount,
        quantity: 1,
        title: `Contribucion al proyecto: ${this.adventureTitle}`,
        type: 'contribution',
        url: `/adventure/${this.adventureInvcode}/${this.adventureSlug}`,
        _id: 'contribution'
      }];

      this.router.navigate(['/checkout'], { queryParams: { item: JSON.stringify(item) }});
    } else {
      this.router.navigate(['/sign'], { replaceUrl: true });
    }
  }
}
