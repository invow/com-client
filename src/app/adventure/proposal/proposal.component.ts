import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { CreateService } from '../../create/create.service';
import { AdventureService } from '../../shared/adventure.service';
import { FormsService } from '../../shared/forms/forms.service';
import { SharedService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-proposal',
  templateUrl: './proposal.component.html',
  styleUrls: ['./proposal.component.scss']
})
export class ProposalComponent implements OnInit {
  @Input() adventureProposals: any = null;
  @Input() adventureId: string = null;
  @Input() currentInvcode: string = null;
  @Input() currentSlug: string = null;
  @Input() username: string = null;
  @Input() refreshReviews: Function = null;

  proposalForm: FormGroup;

  proposals: any;

  proposalFormView: boolean;
  proposalSuccess: boolean;
  proposalError: boolean;
  isLoading: boolean


  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private createService: CreateService,
      private adventureService: AdventureService,
      private formsService: FormsService,
      private sharedService: SharedService
  ) {
    this.createProposalForm();
  }

  ngOnInit() {
    this.isLoading = false;
    this.proposals = this.adventureProposals;
    this.proposalSuccess = false;
    this.proposalError = false;
    this.proposalFormView = true;
  }

   /**
   * API MESSAGE
   * @param field 
   */
  apiMessage(field: string) {
    switch(field) {
      case 'proposalOk':
      this.proposalFormView = false;
      this.proposalError = false;
      this.proposalSuccess = true;
      break;
      case 'proposalErr':
      this.proposalFormView = false;
      this.proposalSuccess = false;
      this.proposalError = true;
      break;
    }
    setTimeout(() => {
      switch(field) {
        case 'proposalOk':
        this.proposalForm.reset();
        this.proposalError = false;
        this.proposalSuccess = false;
        this.proposalFormView = true;
        break;
        case 'proposalErr':
        this.proposalError = false;
        this.proposalSuccess = false;
        this.proposalFormView = true;
      }
    }, 6000);

  }

   /**
   * SAVE
   * @param type? {String}
   * @param content? {*}
   * @param index? {String}
   */
  save(
    type?: string,
    content?: any,
    index?: string
  ) {
    if(this.username) {
      this.isLoading = true;
      let toSend: any;
      if(!content) {
          this.proposals.push(this.proposalForm.value);
          toSend = {
            proposals: this.proposals
          };
      }
      //after save:
      this.createService.putStepTwo(this.adventureId, toSend)
      .subscribe(
        res => {
          this.isLoading = false;
          this.apiMessage('proposalOk');
          this.adventureService.getByInvcode(this.currentInvcode)
              .subscribe(
                  res => {
                      if(this.currentSlug === res.slug) {
                        this.proposals = this.adventureProposals ? this.adventureProposals : [];
                      }
                      else { alert("INCORRECT URL") }
                  },
                  err => {
              this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                  }
              )

        },
        err => {
          if(err && err.proposals) {
            this.apiMessage('proposalError');
          }
        }
      );
    } else {
      this.router.navigate(['/sign']);
    }
  }
  /**
   * CREATE PROPOSAL FORM
   */
  createProposalForm() {
    this.proposalForm = this.formsService.createProposal();
  }

  /**
   * @GETTERS
   * @Forms
   */
  get proposalFullName() { return this.proposalForm.get('fullName'); }
  get proposalEmail() { return this.proposalForm.get('email'); }
  get proposalContent() { return this.proposalForm.get('proposal'); }
}
