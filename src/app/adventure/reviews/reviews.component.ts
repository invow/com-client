import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdventureService } from '../../shared/adventure.service';
import { CreateService } from '../../create/create.service';
import { FormGroup } from '@angular/forms';
import { FormsService } from '../../shared/forms/forms.service';
import { SharedService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {
  @Input() adventureReviews: any = null;
  @Input() adventureId: string = null;
  @Input() currentSlug: string = null;
  @Input() currentInvcode: string = null;
  @Input() currentUser: boolean = null;
  @Input() refreshReviews: Function = null;

  @Output() getReview = new EventEmitter<string>();

  reviewForm: FormGroup;
  reviews: any;
  reviewsView: any;

  reviewFormView: boolean;
  reviewSuccess: boolean;
  reviewError: boolean;

  reviewLoading: boolean;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private adventureService: AdventureService,
      private createService: CreateService,
      private formsService: FormsService,
      public sharedService: SharedService
  ) {
    this.createReviewForm();
  }

  ngOnInit() {
    this.reviewSuccess = false;
    this.reviewError = false;
    this.reviewFormView = true;
    if(!this.reviews) this.reviews = this.adventureReviews;
    if(!this.reviewsView) this.reviewsView = this.reviews;
  }

  /**
   * Emit Review value to Adventure Component
   * @param value String
   */
   emitReview(value: string) {
    this.getReview.emit(value);
  }

  /**
   * CREATE REVIEW FORM
   */
  createReviewForm() {
    this.reviewForm = this.formsService.createReview();
  }

  /**
   * API MESSAGE
   * @param field 
   */
  apiMessage(field: string) {
    switch(field) {
      case 'reviewOk':
      this.reviewFormView = false;
      this.reviewError = false;
      this.reviewLoading = false;
      this.reviewSuccess = true;
      break;
      case 'reviewError':
      this.reviewFormView = false;
      this.reviewSuccess = false;
      this.reviewLoading = false;
      this.reviewError = true;
      break;
    }
    setTimeout(() => {
      switch(field) {
        case 'reviewOk':
        this.reviewForm.reset();
        this.reviewError = false;
        this.reviewSuccess = false;
        this.reviewLoading = false;
        this.reviewFormView = true;
        break;
        case 'reviewError':
        this.reviewError = false;
        this.reviewSuccess = false;
        this.reviewFormView = true;
        break;
      }
    }, 6000);
    

  }

  putStepTwo(toSend: any, vm: any) {
    //after save:
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(
      res => {
        this.reviewLoading = false;
        this.adventureService.getByInvcode(this.currentInvcode)
            .subscribe(
                res => {
                    if(vm.currentSlug === res.slug) {
                      vm.adventureService.replaceLocalAdventure(vm.currentInvcode, res);
                      vm.emitReview(vm.reviews);
                      vm.apiMessage('reviewOk')
                    } else alert("INCORRECT URL")
                },
                err => {
                  this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                }
            )

      },
      err => {
        if(err && err.reviews) {
          this.apiMessage('reviewError');
        }
      }
    );
  }

  /**
   * SAVE
   * @param type? {String}
   * @param content? {*}
   * @param index? {String}
   */
  save(
    type?: string,
    content?: any,
    index?: string
  ) {
    const vm = this;
    this.reviewLoading = true;
    this.reviewFormView = false;
    let toSend: any;
    this.reviewsView = [];
    this.adventureService.getCurrentUser().subscribe(res => {
      const username = res.name;
      vm.reviewForm.controls.username.setValue(username);
      vm.reviews.push(vm.reviewForm.value);
      this.reviewsView = vm.reviews;
      toSend = { reviews: vm.reviews };
      this.putStepTwo(toSend, this);
    });
  }
  /**
   * @GETTERS
   * @Forms
   */
  get reviewTitle() { return this.reviewForm.get('title'); }
  get reviewStars() { return this.reviewForm.get('stars'); }
  get reviewEmail() { return this.reviewForm.get('email'); }
  get reviewContent() { return this.reviewForm.get('review'); }
}
