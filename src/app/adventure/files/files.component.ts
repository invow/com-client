import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { SharedService } from '../../shared/services/shared.service';
import { CreateService } from '../../create/create.service';
import { AdventureService } from '../../shared/adventure.service';
import { FormsService } from '../../shared/forms/forms.service';
import { AccountService } from '../../shared/services/account.service'

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
  @Input() adventureDocuments: any = null;
  @Input() adventureId: string = null;
  @Input() currentInvcode: string = null;
  @Input() currentSlug: string = null;

  fileUploaded: boolean;
  fileRequired: boolean;
  fileExtension: boolean;
  fileSize: boolean;
  fileState: string;
  uploadingState: string;
  uploadState: string;
  documentForm: FormGroup;
  documents: any;
  documentFormView: boolean;
  documentSuccess: boolean;
  documentError: boolean;
  domain: string;
  
  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private accountService: AccountService,
      public sharedService: SharedService,
      private createService: CreateService,
      private adventureService: AdventureService,
      private formsService: FormsService
  ) {
    this.createDocumentForm();
  }

  ngOnInit() {
    this.domain = this.sharedService.getDomain();

    this.documentSuccess = false;
    this.documentError = false;
    this.documentFormView = true;
    this.uploadState = 'Subir una imagen';

    this.fileUploaded = false;
    this.fileExtension = false;
    this.fileRequired = false;
    this.fileState = 'Upload!';

    this.uploadingState = 'Uploading';

    const adventure = this.adventureService.getByInvcodeStorage(this.currentInvcode);
    this.sharedService.isLoading = false;
    if(this.currentSlug === adventure.slug && adventure.ready) {
      this.documents = this.adventureDocuments ? this.adventureDocuments : [];
    }
  }

   /**
   * SAVE DOCUMENT
   */
  saveDocument() {
    let invalidCtrls: string[] = this.sharedService.findInvalidControls(this.documentForm);
    if(this.documentForm.invalid || invalidCtrls.length !== 0) {
        if(invalidCtrls.length !== 0) {
          Object.keys(this.documentForm.controls).forEach(field => {
            const control = this.documentForm.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          if(invalidCtrls.indexOf('fileInput') !== -1) {
              this.fileRequired = true;
          }
          if(
              this.fileState === this.uploadingState
          ) {
              alert('Some files still uploading... Please wait a minute');
          }
        }
    } else {
        //TODO: call API
    }
    window.scrollTo(0, 0);
}

/**
   * ON FILE CHANGE
   * @param $event $Event
   */
  onFileChange($event: any) {
    let file = $event.target.files[0]; // <--- File Object for future use.
    if(
        $event.target.files &&
        file &&
        this.sharedService.validateFile('doc', file.name, file.size, 50000000)
    ) {
        this.fileState = this.uploadingState;
        this.documentForm.get('fileInput').setValue(file ? file.name : '');
        this.createService.postFile(file, 'doc')
          .subscribe(
              res => {
                  this.createService.postFileData(
                    file.name,
                    res ? res.name : 'Error while tried to find the name generated in adventure/uploads.',
                    'doc'
                  )
                    .subscribe(
                      res => {
                        console.log('Success!');
                        console.log(res);
                      },
                      err => {
                   this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                      }
                  )
                  this.fileState = file.name;
                  this.documentForm.get('name').setValue(res ? res.name : '');
                  this.fileUploaded = true;
              },
              err => {
             this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
              });
    }
}

  /**
   * CREATE DOCUMENT FORM
   */
  createDocumentForm() {
    this.documentForm = this.formsService.createDocs();
  }

   /**
   * REMOVE
   * @param field {String}
   * @param initialContent {*}
   */
  remove(field: string, initialContent: any) {
    let toSend: any;
    let found = this.adventureDocuments.filter(this.sharedService.findItem);
    this.documents = found;
    toSend = {
      documents: this.documents
    }
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(res => { console.log(res) }, err=> {});
  }

  /**
   * SAVE
   * @param type? {String}
   * @param content? {*}
   * @param index? {String}
   */
  save(
    type?: string,
    content?: any,
    index?: string
  ) {
    this.sharedService.isLoading = true;
    let toSend: any;
    if(!content) {
        this.documents.push(this.documentForm.value);
        toSend = {
          documents: this.documents
        };
    }
    //after save:
    this.createService.putStepTwo(this.adventureId, toSend)
    .subscribe(
      res => {
        this.sharedService.isLoading = false;
        this.adventureService.getByInvcode(this.currentInvcode)
            .subscribe(
                res => {
                    if(this.currentSlug === res.slug) {
                      this.documents = this.adventureDocuments ? this.adventureDocuments : [];
                    }
                    else { alert("INCORRECT URL") }
                },
                err => {
             this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
                }
            )
      },
      err => {
        if(err && err.documents) {
          this.apiMessage('documentError');
        }
      }
    )
  }

  /**
   * API MESSAGE
   * @param field 
   */
  apiMessage(field: string) {
    switch(field) {
      case 'documentOk':
      this.documentFormView = false;
      this.documentError = false;
      this.documentSuccess = true;
      this.fileUploaded = false;
      this.fileState = 'Upload!';
      break;
      case 'documentError':
      this.documentFormView = false;
      this.documentSuccess = false;
      this.documentError = true;
      this.fileUploaded = false;
      this.fileState = 'Upload!';
      break;
    }
    setTimeout(() => {
      switch(field) {
        case 'documentOk':
        this.documentForm.reset();
        this.documentForm.controls.public.setValue(true);
        this.documentError = false;
        this.documentSuccess = false;
        this.documentFormView = true;
        break;
        case 'documentError':
        this.documentError = false;
        this.documentSuccess = false;
        this.documentFormView = true;
        break;
      }
    }, 6000);

  }

  /**
   * @GETTERS
   * @Forms
   */

  get documentType() { return this.documentForm.get('type'); }
  get documentName() { return this.documentForm.get('name'); }
  get documentSize() { return this.documentForm.get('size'); }
  get documentPublic() { return this.documentForm.get('public')}
  get fileInput() { return this.documentForm.get('fileInput'); }
}
