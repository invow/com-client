import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../shared/services/shared.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  @Input() width: string = "100%";
  @Input() height: string = "300";
  @Input() urlEmbed: string = null;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      public sharedService: SharedService
  ) { }

  ngOnInit() {  }
  

}
