import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from '../core/authentication/authentication.guard';
import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { ProfileComponent } from './profile.component';

const routes: Routes = Route.withShell([
  {
    path: 'profile/:username',
    component: ProfileComponent,
    canActivate: [AuthenticationGuard],
    data: { title: extract('Profile') }
  }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ProfileRoutingModule { }
