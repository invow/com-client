import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdventureService } from '../shared/adventure.service';
import { FormsService } from '../shared/forms/forms.service';
import { ProfileService } from './profile.service';
import { ProfileFormsService } from '../shared/forms/profile.forms.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { CreateService } from '../create/create.service';
import { SharedService } from '../shared/services/shared.service';
import { Account } from '../shared/classes/account';
import { AccountService } from '../shared/services/account.service';
import { AuthenticationService } from '../core/authentication/authentication.service';

import {
    FormArray,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn,
    FormControl
} from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
  @ViewChild("reviewsTarget") reviewsTarget: any;
  user: any;
  accounts: Account[]
  status: any;
  adventures: Array<any>;
  isLoading: boolean;
  profile: any;
  username: string;
  showLogout: boolean;
  profileForm: FormGroup;
  showBioInput: boolean;
  showBioEditButton: boolean;
  uploadingState: string;
  reviews: any;
  reviewFormView: boolean;
  reviewSuccess: boolean;
  reviewError: boolean;
  profileReviewsResult: number;
  showPublicationForm: boolean;
  proposals: any;
  contributions: any;
  delivery: any;
  rewards: any;
  contact: any;
  shippingCompany: string;
  trackingCode: string;
  adventureIndex: number;
  deliveryIndex: number;
  rewardIndex: number;
  shippingFormModal: any;
  statusShippingModal: any;
  checkoutDatesRewards: Array<String> = [];
  checkoutDatesDelivery: Array<String> = [];
  showNew: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private adventureService: AdventureService,
    private formsService: FormsService,
    private profileService: ProfileService,
    private accountService: AccountService,
    private profileFormsService: ProfileFormsService,
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
    private createService: CreateService,
    private sharedService: SharedService,
    private authenticationService: AuthenticationService
  ) {
    //window.scrollTo(0, 0);
    this.createProfileForm();
  }

  ngOnInit() {
    this.showLogout = false;
    this.adventures = [];
    this.showNew = false;
    if(this.authenticationService.isAuthenticated()) {
      this.profileService.getCurrentUser()
      .subscribe(
        res => { 
          this.user = res;
          if(this.user.name !== this.username) {
            this.router.navigate([`/error`], { queryParams: { err: JSON.stringify({})}, replaceUrl: true });
          }
        },
        err => {
          this.router.navigate([`/error`], { queryParams: { err: JSON.stringify(err)}, replaceUrl: true });
        }
      );
    } else { this.router.navigate(['/sign'], { replaceUrl: true }) }

    this.status = {
      reward: {
        name: 'pending',
        tag: 'Pendiente de envío.',
        button: true
      },
      sign: {
        name: 'pending',
        tag: 'Pendiente de firma.',
        button: true
      },
      agent: {
        name: 'pending',
        tag: 'En progreso.',
        button: true
      }
    };
    this.isLoading = true;
    this.showBioEditButton = false;
    this.showBioInput = false;
    this.reviewSuccess = false;
    this.reviewError = false;
    this.reviewFormView = true;
    this.profileReviewsResult = 0;

    this.route.params.subscribe((params: Response) => {
      this.username = params['username'] ? params['username'] : null;
    });
    if(this.authenticationService.isAuthenticated()) {
      this.profileService.getAccount(this.username)
      .subscribe(
        res => {
          this.profile = res;
          this.delivery = res.delivery;
          this.rewards = res.rewards;
          if (
            this.rewards &&
            this.delivery &&
            this.rewards.length !== 0 &&
            this.delivery.length !== 0
          ) {
            this.delivery.forEach((item: any, index: number) => {
              const checkoutDate = new Date(item.checkoutDate);
              let day = checkoutDate.getDate();
              let month = checkoutDate.getMonth() + 1;
              let year = checkoutDate.getFullYear();
              if(month < 10){
                this.checkoutDatesDelivery.push(`${day}/0${month}/${year}`);
              }else{
                this.checkoutDatesDelivery.push(`${day}/${month}/${year}`);
              }            
            })
            this.rewards.forEach((item: any) => {
              const checkoutDate = new Date(item.checkoutDate);
              let day = checkoutDate.getDate();
              let month = checkoutDate.getMonth() + 1;
              let year = checkoutDate.getFullYear();
              if(month < 10){
                this.checkoutDatesRewards.push(`${day}/0${month}/${year}`);
              }else{
                this.checkoutDatesRewards.push(`${day}/${month}/${year}`);
              }            
            })
          }
          if(this.profile.adventures !== []) {
            this.profile.adventures.forEach((adventure: any, index: number) => {
            const adv = this.adventureService.getByInvcodeStorage(adventure);
            if(adv) {
              const createdAt = new Date(adv.createdAt);
              let day = createdAt.getDate();
              let month = createdAt.getMonth() + 1;
              let year = createdAt.getFullYear();
              if(month < 10){
                adv.createdAt = `${day}/0${month}/${year}`;
              }else{
                adv.createdAt = `${day}/${month}/${year}`;
              }
              this.proposals = adv.proposals;
              this.delivery = adv.delivery;
              if(this.delivery && this.delivery.length !== 0) {
                this.delivery.forEach((item: any) => {
                const checkoutDate = new Date(item.checkoutDate);
                let day = checkoutDate.getDate();
                let month = checkoutDate.getMonth() + 1;
                let year = checkoutDate.getFullYear();
                if(month < 10){
                  this.checkoutDatesDelivery.push(`${day}/0${month}/${year}`);
                }else{
                  this.checkoutDatesDelivery.push(`${day}/${month}/${year}`);
                }            
              })
              }
              this.rewards = adv.rewards;
              this.contributions = adv.contributions;
              this.adventures.push(res);
            } else this.showNew = true;
            })
          }
        },
        err => {
          this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
        });
      this.accountService.account()
      .subscribe(
        res => {
          this.accounts = res;
          this.isLoading = false;
        },
        err => {
          this.router.navigate(['/error'], { queryParams: { err: JSON.stringify(err) }});;
        }
      );
    }
  }

  get firstName() { return this.profileForm.get('firstName'); }
  get lastName() { return this.profileForm.get('firstName'); }
  get shortBio() { return this.profileForm.get('shortBio'); }

  setContactModalData(htmlInsideModal: any, deliv: any, ai: number, di: number) {
    this.contact = {
      ai: ai,
      di: di,
      contributor: deliv.contributor,
      phone: deliv.phone,
      email: deliv.email
    }    
    htmlInsideModal.open();
  }

  loadShippingMethod(aindex: number, dindex: number) {
    this.adventures[aindex].delivery[dindex].shipCompany = this.shippingCompany;
    this.adventures[aindex].delivery[dindex].shipTrackingCode = this.trackingCode;
    this.adventures[aindex].delivery[dindex].status = 'sent';
    this.createService.putStepTwo(this.adventures[aindex].id, { delivery: this.adventures[aindex].delivery})
    .subscribe(
      res => {
        if(res) {
          this.shippingFormModal.close();
          this.shippingCompany = '';
          this.trackingCode = '';
          this.profile.forEach((item: any, index: number) => {
            this.profile.rewards[index].status = 'sent';
            this.profile.rewards[index].shipCompany = this.shippingCompany;
            this.profile.rewards[index].shipTrackingCode = this.trackingCode;
          })
          //this.profileService.putAccount(this.profile.id, { rewards: this.profile.rewards })
        }
      },
      err => {
        this.router.navigate([`/error?err=${err}`], { replaceUrl: true })
      }
    );
  }

  setShippingContactModalData(htmlInsideModal: any, deliv: any) {
    this.contact = {
      fullName: `${deliv.firstName} ${deliv.lastName}`,
      address: deliv.address,
      phone: deliv.phone,
      email: deliv.email
    }    
    htmlInsideModal.open();
  }

  createProfileForm() {
      this.profileForm = this.profileFormsService.createAccount();
  }

  editShipping() {
    //this.showEditInputs = this.showEditInputs ? false : true;
  }

  scroll(el: any) {
    el.nativeElement.scrollIntoView();
  }

  changeRewardStatus() {
    this.status.reward.name = 'success';
    this.status.reward.tag = 'Recibido.';
    this.status.reward.button = false;
  }

  changeSignStatus() {
    this.status.sign.name = 'success';
    this.status.sign.tag = 'Firmado';
    this.status.sign.button = false;
  }

  changeAgentStatus() {
    this.status.agent.name = 'success';
    this.status.agent.tag = 'Finalizado.';
    this.status.agent.button = false;
  }

  /**
   * CALCULATE DAYS
   * @param createdAt
   * @param endDate
   */
  calculateDays(endDate: Date) {
    const diffTime = Math.abs(
      new Date(endDate).getTime() -
      new Date().getTime()
    );

    const days = Math.ceil(
      diffTime /
      (1000 * 60 * 60 * 24)
    );
    return days;
  }

  setShippingFormModal(shippingFormModal: any, ai: number, di: number) {
    this.adventureIndex = ai;
    this.deliveryIndex = di;
    this.shippingFormModal = shippingFormModal;
    shippingFormModal.open();
  }

  setStatusShippingModal(statusShippingModal: any, index: number) {
    this.rewardIndex = index;
    this.statusShippingModal = statusShippingModal;
    statusShippingModal.open();
  }

  delivered() {
    this.profile.rewards[this.rewardIndex].status = 'delivered';
    this.profileService.putAccount(this.profile.id, { rewards: this.profile.rewards })
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }

    )
    this.statusShippingModal.close();
  }

  logout() {
    localStorage.clear();
    document.location.href = '/';
  }

}
