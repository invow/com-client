import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { CartComponent } from './cart.component';

const routes: Routes = Route.withShell([
  { path: 'cart', component: CartComponent, data: { title: extract('Cart') } }
]);
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class CartRoutingModule { }
