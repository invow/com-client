import { Component, OnInit } from '@angular/core';
import { CartItem } from '../shared/classes/cart-item'
import { CartService } from '../shared/services/cart.service'
import { SharedService } from '../shared/services/shared.service'

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  items : Array<any> = [];
  
  constructor(
    public cartService: CartService,
    private sharedService: SharedService
  ) { }
  ngOnInit() {}
}
