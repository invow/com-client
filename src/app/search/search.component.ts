import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

interface Adventures {
    title: string;
    img: string;
    oldPrice: string;
    price: string;
    stars: number;
    funded: boolean;
    wishList: boolean;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
    q: string;
    adventures: Array<Adventures>;
    adventuresByTitle: Array<Adventures>;

    constructor(
      private router: Router,
      private route: ActivatedRoute
    ) { }

    ngOnInit() {
      this.route.queryParams.subscribe((params: Response) => {
          this.q = params['q'];
      });

      //Mocks
      this.adventures = [
          {
              title: 'Storage Box',
              img: 'th01.jpg',
              oldPrice: '490',
              price: '380',
              stars: 0,
              funded: true,
              wishList: true
          },
          {
              title: 'Shoulder Bug',
              img: 'th02.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Glass Vase',
              img: 'th03.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Alarm Clock',
              img: 'th04.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 4,
              funded: true,
              wishList: false
          },
          {
              title: 'Storage Box',
              img: 'th13.jpg',
              oldPrice: '490',
              price: '380',
              stars: 0,
              funded: true,
              wishList: true
          },
          {
              title: 'Shoulder Bug',
              img: 'th14.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Glass Vase',
              img: 'th15.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Alarm Clock',
              img: 'th16.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Storage Box',
              img: 'th05.jpg',
              oldPrice: '490',
              price: '380',
              stars: 0,
              funded: true,
              wishList: true
          },
          {
              title: 'Shoulder Bug',
              img: 'th06.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 5,
              funded: true,
              wishList: false
          },
          {
              title: 'Glass Vase',
              img: 'th07.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Alarm Clock',
              img: 'th08.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Storage Box',
              img: 'th09.jpg',
              oldPrice: '490',
              price: '380',
              stars: 0,
              funded: true,
              wishList: true
          },
          {
              title: 'Shoulder Bug',
              img: 'th10.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Glass Vase',
              img: 'th11.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          },
          {
              title: 'Alarm Clock',
              img: 'th12.jpg',
              oldPrice: '4090',
              price: '3800',
              stars: 0,
              funded: true,
              wishList: false
          }
      ];
      this.adventuresByTitle = this.adventures.filter(
         adventure => adventure.title.toLowerCase().indexOf(this.q.toLowerCase()) !== -1 );
    }

}
