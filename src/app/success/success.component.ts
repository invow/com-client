import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
  invop: any;
  operation: boolean;
  newAdventure: boolean;
  invcode: string;
  slug: string;
  password: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Response) => {
      if(params['order']) {
        const order = JSON.parse(params['order']);
        this.invop = order.invop ? order.invop : null;
        if(order.invcode) {
          this.newAdventure = true;
          this.invcode = order.invcode;
          this.slug = order.slug;
        }
        if(this.invop)
          this.operation = true;
      } else if(params['password']) {
        this.password = true;
      } else {
          this.router.navigate([`/`], { replaceUrl: true });
      }
  });
    setTimeout(() => {
      if(this.newAdventure)
        this.router.navigate([`/adventure/${this.invcode}/${this.slug}`], { replaceUrl: true });
      else 
        this.router.navigate([`/`], { replaceUrl: true });
    }, 6000)
  }


}
