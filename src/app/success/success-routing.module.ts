import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { extract } from '../core/i18n.service';
import { SuccessComponent } from './success.component';

const routes: Routes = Route.withShell([
  { path: 'success', component: SuccessComponent, data: { title: extract('Success') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class SuccessRoutingModule { }
