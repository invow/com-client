import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgProgressModule } from 'ngx-progressbar';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './home/home.module';
import { AboutModule } from './about/about.module';
import { LoginModule } from './login/login.module';
import { SignModule } from './sign/sign.module';
import { CartModule } from './cart/cart.module';
import { FaqModule } from './faq/faq.module';
import { SearchModule } from './search/search.module';
import { AdventureModule } from './adventure/adventure.module';
import { ProfileModule } from './profile/profile.module';
import { CheckoutModule } from './checkout/checkout.module';
import { CreateModule } from './create/create.module';
import { TermsModule } from './terms/terms.module';
import { ReportModule } from './report/report.module';
import { ErrorModule } from './error/error.module';
import { SuccessModule } from './success/success.module';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { ResetPasswordModule } from './reset-password/reset-password.module';
import { PriceModule } from './price/price.module';
import { ChartsModule } from 'ng2-charts';
import { PipeModule } from './shared/pipes/pipe.module'; // import our pipe here
import { ModalModule } from 'angular-custom-modal';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    NgbModule.forRoot(),
    NgProgressModule,
    CoreModule,
    ChartsModule,
    SharedModule,
    HomeModule,
    AboutModule,
    LoginModule,
    SignModule,
    CartModule,
    FaqModule,
    SearchModule,
    CheckoutModule,
    CreateModule,
    TermsModule,
    ReportModule,
    ErrorModule,
    SuccessModule,
    ForgotPasswordModule,
    ResetPasswordModule,
    PriceModule,
    AdventureModule,
    ProfileModule,
    PipeModule,
    ModalModule,
    AppRoutingModule
  ],
  declarations: [AppComponent],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
