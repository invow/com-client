// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
const local = 'http://localhost:9001/api';
const dev = 'https://invow-com.azurewebsites.net/api';
const assets = 'https://invow.blob.core.windows.net/invow-container'
const env = local;

export const environment = {
  production: false,
  version: '(dev)',
  serverUrl: env,
  assetsUrl: assets,
  defaultLanguage: 'es-AR',
  supportedLanguages: [
    'es-AR'
  ]
};
